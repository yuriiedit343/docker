# docker
## @edt ASIX-M05 Curs 2021-2022

# Servidor web detach

Els containers Docker es poden fer servir per a moltes coses, fins ara hem vist que per exemple els podem usar per practicar en un entorn segur ordres de GNU/Linux. O també  per corregir exercicis de python o scripts que s’executen com a root en entorns segurs. Per practicar xarxes, serveis, etc

Però el proposit principal dels containers es deplegar **microserveis**. Crear imatges que permetran desplegar containers on a cada container s’executa un serveis. Res prohibeix d’executar més d’un servei en un container, però va en contra de la filosofia de funcionament. 

La idea és que el container és el més petit possible i conté només el necessari per desplegar un servei, per exemple un servei web (o un servei ldap o un servei ftp o un servei saba, etc). Un sol servei en un container. 

Els serveis co per exemple el servei web s’activa normalment en **background** amb l’ordre systemctl. En el cas dels containers l’objectiu és poder desplegar containers que es quedin en execució també en background. Quan un container engega i es queda en funcionament en background diem que és un container **detach**, deslligat de la consola.

Cal tenir present els següents conceptes per treballar en containers detach:

  * Un container es maté en execució mentre el procés amb el que s’ha creat està en execució (la ordre que posem al final del docker run).

  * El procés corresponent al CMD o ordre amb que arrenca el container (la del docker run) és el PID 1. Per tant un container es manté en execució mentre ho fa el procés amb el PID 1.

  * Si el procés acaba el container també.

  * **Problema-1**: normalment els serveis que engegem amb la ordre systemctl start nomservei s’engegen en background i tornen la consola a l’usuari (tèncicament fan un fork i es deslligen de la consola). 
Si això ho fem dins el container (engegar el servei en background) el servei s’executa en background i torna la consola, per tant el procés amb PID 1 finalitza (perquè el dimoni en fer el fork agafa un altre PID) i per tant el container acaba.

  * **Solució**: els serveis, els dimonis, en un container sempre s’han d’executar en foregriund, per tal de mantenir-se en execució en primer pla, com a PID 1 i mantenint viu el container.
És al món al revés. En un GNU/Linux els serveis s’executen en backgriund i dins d’un container s’ha d’executar en foreground (almenys el del PID 1).
Normalment tenim la dificultat de saber com engegar els dimonis en foreground. Cal consultar el man de cada servei segons la distribució de linux usada, no és la mateixa opció per debian que per fedora o ubuntu.

  * **Problema-2**: les imatges estàndard de sistemes operatius GNU/Linux són tant retallades que no incorporen systemctl ni s’hi pot instal·lar (no tindria sentit). Per tant tenim també el problema que per engegar els serveis no podem fer la còmoda ordre systemctl start servei.

  * **Solució**: cal mirar el man de cada servei i identificar quin és l’executable. Generalment en debian podem usar /etc/init.d/nomservei start [opció-foreground]. Podem mirar en el paquet del servei i mostrar el contingut del fitxer /etc/systemd/system/nomservei.service i identificar quin és l’executable. Llavors consultar el man per saber l’opció per enegegar-lo en foreground.


## Servidor Apache detach amb debian

Anem, doncs, a crear un servidor web basat en debian i usant apache. Ens cladrà:

  * Crear el directori de desenvolupament del projecte, que serà el directori de context on hi haurà el Dockerfile i els fitxers necessaris.
  * Generar el Dockerfile.
  * Generar una pàgina web de benvinguda.
  * Identificar l’ordre per activar apache en foreground.

Prodedim a fer la imatge:

  * Crear el directori de treball.

```
$ mkdir myweb
$ cd myweb/
$ pwd
/var/tmp/m05/docker/dockefiles/myweb
```

  * Generar el Dockerfile. S’hi instal·len tot de paquets que no fan falta, només caldria el de apache2, però és per poder depurar i examinar el container si no ens funciona bé. Un cop verificat tot aquestes paquets es podrien eliminar del Dockerfile per fer la imatge més lleugera.

```
# servidor web basat en debian:latest usant apache 
FROM debian:latest
LABEL author="@edt ASIX 2022"
LABEL curs="docker /AWS"
RUN apt-get update && apt-get install -y procps iproute2 iputils-ping nmap tree vim apache2
COPY index.html /var/www/html/
WORKDIR /tmp
EXPOSE 80
CMD  apachectl start 
```

  * Generar la imatge amb docker build.

```
$ docker build -t edtasixm05/myweb:latest .
Sending build context to Docker daemon  2.048kB
Step 1/8 : FROM debian:latest
 ---> 82bd5ee7b1c5
Step 2/8 : LABEL author="@edt ASIX 2022"
 ---> Running in 750adee16265
Removing intermediate container 750adee16265
 ---> 122786d334cc
Step 3/8 : LABEL curs="docker /AWS"
 ---> Running in 1d90f387448f
Removing intermediate container 1d90f387448f
 ---> baa658a108d1
Step 4/8 : RUN apt-get update && apt-get install -y procps iproute2 iputils-ping nmap tree vim apache2
 ---> Running in cd85b3d60b6d
Get:1 http://deb.debian.org/debian bullseye InRelease [116 kB]
…
Step 5/8 : COPY index.html /var/www/html/
COPY failed: file not found in build context or excluded by .dockerignore: stat index.html: file does not exist
```

  * El build ha fallat perquè no hem pensat a generar el fitxer amb la pàgina web de benvinguda index.hmtl que utilitza el Dockerfile. 

  * Generem el fitxer amb la pàgina web. Podeu fer-la tant sofisticada com volgueu però aquí farem trampeta… recordeu que el directori de publicació d’apache és **/var/www/html** i per defecte servei la pàgina anomenada **index.html**.

```
$ echo "pagina web de benvinguda, hola gent!" > index.html

$ ls
Dockerfile  index.html
```

  * Tornem a fer el docker build.

```
$ docker build -t edtasixm05/myweb:latest .
Sending build context to Docker daemon  3.072kB
Step 1/8 : FROM debian:latest
 ---> 82bd5ee7b1c5
Step 2/8 : LABEL author="@edt ASIX 2022"
 ---> Using cache
 ---> 122786d334cc
Step 3/8 : LABEL curs="docker /AWS"
 ---> Using cache
 ---> baa658a108d1
Step 4/8 : RUN apt-get update && apt-get install -y procps iproute2 iputils-ping nmap tree vim apache2
 ---> Using cache
 ---> 261e46843676
Step 5/8 : COPY index.html /var/www/html/
 ---> a91bb8b675d0
Step 6/8 : WORKDIR /tmp
 ---> Running in ef8da99b3b71
Removing intermediate container ef8da99b3b71
 ---> 14b3fefeef2e
Step 7/8 : EXPOSE 80
 ---> Running in b99b09344b51
Removing intermediate container b99b09344b51
 ---> 85bcbb33b845
Step 8/8 : CMD  apachectl -k start
 ---> Running in 7488716f0c57
Removing intermediate container 7488716f0c57
 ---> 8073c1fe3760
Successfully built 8073c1fe3760
Successfully tagged edtasixm05/myweb:latest
```

  * Executar un container en mode detach, el container s’executa en background. L’ordre és docker run però cal usar l’opció -d de detach en lloc de l’opció -it usada fins ara. Ara ja no volem tenir un terminal i treballar interactivament en el container.

  * Un cop engegat verificar amb docker ps que s’ha quedat funcionant en detach el container.

```
$ docker run --rm -d edtasixm05/myweb:latest 
a40bd3c8236bc0501f9d164b154363f14b9c4533e6b5d695819d9c39b9fd6419

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

  * **Error!**. L’ordre docker run ha generat el container i ens ha indicat el seu ID, però en verificar si està funcionant veiem que no. S’ha engegat però ha finalitzat i s’ha destruit immediatament!
    **Atenció!**: Quan ens passa això podem assumir que és que el servei que hem engegat ha finalitzat la seva execició i per tant el container també ha finalitzat. No hem fet bé engegar el servei en foreground. Comprovem-ho.

  * Per verificar la creació d’imatges que no ens acaben d’anar bé podem engegar un container interactivament i repetir manualment dins el container les ordres que haurien de funcionar i veure si ho fan o no.

  * Poseu atenció que si ara volem treballar interactivament en el container per depurar-lo ens cal: 
    * Posar -it
    * Indicar que volem un shell amb /bin/bash, per exemple.

```
$ docker run --rm -d edtasixm05/myweb:latest /bin/bash
bcab1c89bddf9be5b9d923c7c5512bbff43b2df13fe56269ba2ca3e6fbde4c9b
```

  * Punyeta! Ha fallat perquè encara diu -d en lloc de -it.

  * Fem-ho ara bé.
    * Entrem interactivament en el container.
    * Fem la ordre que hem fet per engegar el dimoni, el servei de apache: apachectl -k start.
    * Com que hem instal·lat paquets per poder depurar veiem que ara el servei està engegat i que el port 80 està obert.
    * Però fixem-nos que ens ha retornat el prompt, per tant el servei s’executa en background. Això hem quedat que no pot ser en un container detach. El servei s’ha d’executar en foreground.
 
```
$ docker run --rm -it edtasixm05/myweb:latest /bin/bash

root@163ca355ff9b:/tmp# apachectl -k start
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message

root@163ca355ff9b:/tmp# ps ax
    PID TTY      STAT   TIME COMMAND
      1 pts/0    Ss     0:00 /bin/bash
     17 ?        Ss     0:00 /usr/sbin/apache2 -k start
     18 ?        Sl     0:00 /usr/sbin/apache2 -k start
     19 ?        Sl     0:00 /usr/sbin/apache2 -k start
     74 pts/0    R+     0:00 ps ax

root@163ca355ff9b:/tmp# nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 18:07 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0000010s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

root@163ca355ff9b:/tmp# exit
exit
```

  * Repetim el procés (sortim i tornem a generar el container) i anem a investigar el man de l’ordre apachectl per veure com engegar el servidor en foreground.
    * Consultant el man hem vist que cal passar-li el paràmetre -X per executar-se en foreground.
    * Veiem que ara en engegar el servei no ens torna el prompt, per tant el servei s’està executant en primer pla.
    * Podem ignorar el warning que mostra, simplement informa que no s’ha definit un nom formal per al servidor web.

```
$ docker run --rm -it edtasixm05/myweb:latest /bin/bash

root@d2a307d567c0:/tmp# apachectl -k start -X
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message
```

  * Com que som escèptics de mena volem comprovar que realment el servei està engegat: entrarem dins el container des d’una altra sessió  i verificarem els processos i el port.

```
$ docker exec -it vigilant_chaum /bin/bash

root@d2a307d567c0:/tmp# ps ax
    PID TTY      STAT   TIME COMMAND
      1 pts/0    Ss     0:00 /bin/bash
      7 pts/0    S+     0:00 /bin/sh /usr/sbin/apachectl -k start -X
     16 pts/0    Sl+    0:00 /usr/sbin/apache2 -k start -X
     44 pts/1    Ss     0:00 /bin/bash
     51 pts/1    R+     0:00 ps ax

root@d2a307d567c0:/tmp# nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 18:12 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0000020s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

root@d2a307d567c0:/tmp# exit
exit
```

  * Sembla que finalment hem trobat com engegar correctament el servei. Anem a editar el Dockerfile per actualitzar-lo i generar la imatge de nou. Recordeu també tancar el container que teniem engegat (ĉ i exit o des d’una altra consola docker stop).

```
# servidor web basat en debian:latest usant apache 
FROM debian:latest
LABEL author="@edt ASIX 2022"
LABEL curs="docker /AWS"
RUN apt-get update && apt-get install -y procps iproute2 iputils-ping nmap tree vim apache2
COPY index.html /var/www/html/
WORKDIR /tmp
EXPOSE 80
CMD  apachectl -k start -X 
```

```
$ docker build -t edtasixm05/myweb:latest .
Sending build context to Docker daemon  3.072kB
Step 1/8 : FROM debian:latest
 ---> 82bd5ee7b1c5
Step 2/8 : LABEL author="@edt ASIX 2022"
 ---> Using cache
 ---> 122786d334cc
Step 3/8 : LABEL curs="docker /AWS"
 ---> Using cache
 ---> baa658a108d1
Step 4/8 : RUN apt-get update && apt-get install -y procps iproute2 iputils-ping nmap tree vim apache2
 ---> Using cache
 ---> 261e46843676
Step 5/8 : COPY index.html /var/www/html/
 ---> Using cache
 ---> a91bb8b675d0
Step 6/8 : WORKDIR /tmp
 ---> Using cache
 ---> 14b3fefeef2e
Step 7/8 : EXPOSE 80
 ---> Using cache
 ---> 85bcbb33b845
Step 8/8 : CMD  apachectl -k start -X
 ---> Running in 07d5c97d60b0
Removing intermediate container 07d5c97d60b0
 ---> 7f38d887b2ca
Successfully built 7f38d887b2ca
Successfully tagged edtasixm05/myweb:latest
```

  * Engegem el container i verifiquem que es queda en execució.

```
$ docker run --rm --name web.edt.org -h web.edt.org -d edtasixm05/myweb
f422147cf4cc84345a0a205cad989b9c2712c76a3ad8e6d3b1561da7e74aca1e

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS     NAMES
f422147cf4cc   edtasixm05/myweb   "/bin/sh -c 'apachec…"   3 seconds ago   Up 2 seconds   80/tcp    web.edt.org
```

  * Anem a verificar que el servidor web està oferint la nostra magnífica pàgina web.
    * Amb wget.
    * Amb telnet.
    * Des d’un navegador.

  * Identificar l’adreça IP del container per exemple amb docker inspect nom del container. Per defecte el primer container té l’adreça 172.17.0.2.

```
$ docker inspect web.edt.org  | grep IP
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
                    "IPAMConfig": null,
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,

$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 20:19 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.00013s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

```
$ wget 172.17.0.2
--2022-07-05 20:20:13--  http://172.17.0.2/
Connecting to 172.17.0.2:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 37 [text/html]
Saving to: ‘index.html.1’

index.html.1                            100%[=============================================================================>]      37  --.-KB/s    in 0s      

2022-07-05 20:20:13 (2.89 MB/s) - ‘index.html.1’ saved [37/37]

$ cat index.html.1 
pagina web de benvinguda, hola gent!

$ rm index.html.1 
```

```
$ telnet 172.17.0.2 80
Trying 172.17.0.2...
Connected to 172.17.0.2.
Escape character is '^]'.
GET / HTTP/1.0  (premeu dos returns)

HTTP/1.1 200 OK
Date: Tue, 05 Jul 2022 18:20:56 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Tue, 05 Jul 2022 17:56:10 GMT
ETag: "25-5e3129149c280"
Accept-Ranges: bytes
Content-Length: 37
Connection: close
Content-Type: text/html

pagina web de benvinguda, hola gent!
Connection closed by foreign host.
```

![image6](images/image6.png)

```
```

  * Fantàstic, el servei web ja funciona en un container en detach que podem desplegar a qualsevol lloc del món si primer el pujem al repositori. Es el moment d’aturar el container i fer una cerveseta!

```
$ docker stop web.edt.org 
web.edt.org

$ docker push edtasixm05/myweb:latest 
The push refers to repository [docker.io/edtasixm05/myweb]
ccc76123a0d4: Pushed 
92565d9c2e53: Pushed 
799760671c38: Mounted from edtasixm05/mydebian 
latest: digest: sha256:b1222afc126d4c7a980c547ad6da49baddb63a75e469baaf6fe3d573aefee07d size: 948

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

En resum

  * Es generen imatges usant fitxers Dockerfile de descripció de la creació.
  * S’utilitza un directori per a desenvolupar cada imatge, que contindrà tot allò necessari, serà el directori de context.
  * Els serveis en el container s’han d’engegar (si són el PID 1) en foreground.
  * Les imatges es generen per oferir un microservei, un per imatge (serà un per container).
  * Les imatges un cop fetes es poden  pujar al DockerHub.
  * Els directoris de creació d’imatges es poden pujar al GIT, d’aquesta manera no cal emmagatzemar la imatge perquè tenim la recepta de com fabricar-la.
  * Per cada imatge d’un servei que volem crear ens cal identificar quins fitxers necessita, com es diu l’executable i quina opció l’engega en foreground.
















