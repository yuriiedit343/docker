# getting started - ITEMS
## @edt ASIX-M05 Curs 2021-2022


### Exemple de Getting Started - 5: Web Items

En aquest exemple es crea una pàgina web on es poden anar anotant ítems. Utilitza
node.js amb JavaScript.


Primerament descarregar la app del git
```
git clone https://github.com/docker/getting-started.git
```

Generar la imatge i fer el deploy. Verificar l'accés. **Atenció!** Cal anar al directory
app on hi ha el fitxer package.json.

```
$ cd getting-started/app/

$ ls
package.json  spec  src  yarn.lock
```

Crear el Dockerfile (dins directori app)
```
# syntax=docker/dockerfile:1
FROM node:12-alpine
RUN apk add --no-cache python2 g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
EXPOSE 3000
```

```
$ docker build -t items .

$ docker run -dp 3000:3000 items

$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                       NAMES
cd40988aa3e7   items     "docker-entrypoint.s…"   9 seconds ago   Up 9 seconds   0.0.0.0:3000->3000/tcp, :::3000->3000/tcp   charming_pasteur
```

```
Visitar:
   http://localhost:3000
```

#### Pas-2: Modificar la app (no interactivament)

Els passos a fer són:

 * Editar el fitxer source *src/static/js/app.js* i modificar el missatge que 
   es mostra quan la llista és buida.

 * Generar de nou la imatge.

 * Desplegar un nou container basat en la imatge nova (cal abans aturar l'anterior sinó
   hi ha un conflicte amb l'apropiació del port 3000).

 * Visualitzar la pàgina web i observar el canvi.

```
$ vim src/static/js/app.js

-                <p className="text-center">No items yet! Add one above!</p>
+                <p className="text-center">You have no todo items yet! Add one above!</p>
```

```
$ docker build -t items .

$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS                                       NAMES
cd40988aa3e7   0e4092f7b01a   "docker-entrypoint.s…"   10 minutes ago   Up 10 minutes   0.0.0.0:3000->3000/tcp, :::3000->3000/tcp   charming_pasteur

$  docker run -dp 3000:3000 items
cfb3906a8e017ac42cca2e9914a1cb1c614978f792f484dedba75581a3318ffb
docker: Error response from daemon: driver failed programming external connectivity on endpoint jolly_austin (0ee4f8b51a015c9db76e92ca969840e55fa6c071502915830d2adb0fab2ba561): Bind for 0.0.0.0:3000 failed: port is already allocated.

$ docker stop charming_pasteur 
charming_pasteur

$  docker run -dp 3000:3000 items
c884c0cbb522207f8ec3d5532db8f8c6fdf9263ad26ede53ca3b5902ff82c43f

$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                       NAMES
c884c0cbb522   items     "docker-entrypoint.s…"   4 seconds ago   Up 3 seconds   0.0.0.0:3000->3000/tcp, :::3000->3000/tcp   funny_maxwell
```

#### Pas-3: Share the application

En aquest pas es tracta de posar a la imatge un nom apropiat per desar-la
al Docker Hub de l'usuari i fer el push. Després de de qualsevol host, per
exemple des de play-with-docker es pot desplegar.

```
$ docker tag items:latest  edtasixm05/getstarted:items
$ docker push edtasixm05/getstarted:items 
The push refers to repository [docker.io/edtasixm05/getstarted]
3de2bead891e: Pushed 
ec5b0c6da5ae: Pushed 
5c9215c6a361: Pushed 
7c9167d7ac74: Pushed 
7f30cde3f699: Mounted from library/node 
fe810f5902cc: Mounted from library/node 
dfd8c046c602: Mounted from library/node 
4fc242d58285: Mounted from library/nginx 
items: digest: sha256:09cb23787749fe92aeaa8b4a189d785ee2bff1acfdcc3f7568818dfd47b60d6e size: 2000
```

```
$ docker search edtasixm05
NAME                    DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
edtasixm05/web21        @edt Curs 2021-2022 ASIX-M01 Exemple bàsic d…   0                    
edtasixm05/net21        @edt Curs 2021-2022 ASIX-M01 Exemple bàsic d…   0                    
edtasixm05/getstarted   @edt Curs 2021-2022 ASIX-M01 Exemples de doc…   0                    
edtasixm05/ssh21        @edt Curs 2021-2022 ASIX-M01 Exemple bàsic d…   0            
```

```
$ docker image ls edtasixm05/
REPOSITORY              TAG         IMAGE ID       CREATED             SIZE
edtasixm05/getstarted   items       df76b178be48   9 minutes ago       404MB
edtasixm05/getstarted   visites2    3606d2ec1da3   About an hour ago   182MB
edtasixm05/getstarted   bboard      7671aa467199   3 hours ago         687MB
edtasixm05/getstarted   comptador   87c8e3c95b10   27 hours ago        159MB
edtasixm05/getstarted   latest      87c8e3c95b10   27 hours ago        159MB
edtasixm05/net21        base        b3bf496b62b2   3 weeks ago         324MB
edtasixm05/web21        base        bdaaa9ada561   4 weeks ago         478MB
```

#### Pas-4: Persistència de dades

Aquest exemple afegeix un volum de dades anomenat todo-db on es desen les
dades dels items que es van afegint. D'aquesta manera encara que s'aturi el 
container les dades persisteixen.

```
$ docker volume create todo-db
todo-db
$ docker volume ls
DRIVER    VOLUME NAME
local     todo-db
```

```
$ docker run -dp 3000:3000 -v todo-db:/etc/todos items
e5318598c04df272e6a46bd6c8cd3af181f7b953a2bdccd0a4fad45c38cb16a7
docker: Error response from daemon: driver failed programming external connectivity on endpoint intelligent_antonelli (b6751d0f3415e177e875a04b08105ec0e9127b6b60b95cb104818b2f5c3e466f): Bind for 0.0.0.0:3000 failed: port is already allocated.

$ docker stop funny_maxwell 
funny_maxwell
 
$ docker run -dp 3000:3000 -v todo-db:/etc/todos items
bb19352c23e27d650e352d6656a80801a293e19455668ad6324661d97e787627

$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                       NAMES
bb19352c23e2   items     "docker-entrypoint.s…"   6 seconds ago   Up 5 seconds   0.0.0.0:3000->3000/tcp, :::3000->3000/tcp   upbeat_gould
```

Aturar el container. Observar que la web no està activa. Engegar de nou el container
i observar que la llista de ítems es manté.

```
$ docker stop upbeat_gould 
upbeat_gould

$ docker run -dp 3000:3000 -v todo-db:/etc/todos items
154149cdbbc75a5f2289155fe65b61ad44fb2c8de093e3924d0843117dd66b4d
``` 

Observar les dades (la bd) dins el container
```
$ docker exec -it relaxed_hertz ls /etc/todos
todo.db
```

Observar la definició del volum i la seva ubicació al host amfitrió
```
$ docker volume inspect todo-db 
[
    {
        "CreatedAt": "2022-06-01T23:51:58+02:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/todo-db/_data",
        "Name": "todo-db",
        "Options": {},
        "Scope": "local"
    }
]
```

Observar el volum des del host amfitrió
```
[root@mylaptop ~]# ls /var/lib/docker/volumes/todo-db/_data/
todo.db
```


#### Pas-5: Usar bind mount per editar interactivamenr la app

En aquest pas es desplega la app amb una ordre *docker run* més complexa on
es fa un *bind mount* del directori de la app en el host amfitrió al directori
de la app en el container. D'aquesta manera els canvis que es fan en al codi
es reflecteixen inmediatament en la app.

 * Primerament executar el conatiner indicant el directori de treball (/app) i
   el bind mount.

 * Editar el fitxer de l'aplicació *src/static/js/app.js* i modficar per exemple
   l'etiqueta del botó "Add Item"
 
 * Refrescar la pàgina i veure qu eel canvi és inmediat, no cal fer res d'especial
   amb el container.

```
$ docker run --rm -dp 3000:3000 \
     -w /app -v "$(pwd):/app" \
     node:12-alpine \
     sh -c "yarn install && yarn run dev"
```

#### Pas-6: Multicontainer apps

En aquest pas en crea una base de dades mysql i la despleguem en la xarxa todo-app.


Desplegar mysql i verificar la connectivitat.

```
docker network create todo-app

docker run -d \
     --network todo-app --network-alias mysql \
     -v todo-mysql-data:/var/lib/mysql \
     -e MYSQL_ROOT_PASSWORD=secret \
     -e MYSQL_DATABASE=todos \
     mysql:5.7

# Verificar connexió a la BD
docker exec -it <mysql-container-id> mysql -u root -p
mysql>  SHOW DATABASES;
mysql> exit

$ docker ps
CONTAINER ID   IMAGE       COMMAND                  CREATED          STATUS          PORTS                 NAMES
66dccbb8be54   mysql:5.7   "docker-entrypoint.s…"   41 seconds ago   Up 37 seconds   3306/tcp, 33060/tcp   friendly_proskuriakova
```

Variables per configurar la app todo Items per accedir al serveu mysql
 * *MYSQL_HOST* - the hostname for the running MySQL server
 * *MYSQL_USER* - the username to use for the connection
 * *MYSQL_PASSWORD* - the password to use for the connection
 * *MYSQL_DB* - the database to use once connected

Engegar el container de la app connectant al mysql
```
docker run -dp 3000:3000 \
   -w /app -v "$(pwd):/app" \
   --network todo-app \
   -e MYSQL_HOST=mysql \
   -e MYSQL_USER=root \
   -e MYSQL_PASSWORD=secret \
   -e MYSQL_DB=todos \
   node:12-alpine \
   sh -c "yarn install && yarn run dev"

$ docker ps
CONTAINER ID   IMAGE            COMMAND                  CREATED         STATUS              PORTS                                       NAMES
92fbafc8c278   node:12-alpine   "docker-entrypoint.s…"   5 seconds ago   Up 4 seconds        0.0.0.0:3000->3000/tcp, :::3000->3000/tcp   nice_hawking
66dccbb8be54   mysql:5.7        "docker-entrypoint.s…"   2 minutes ago   Up About a minute   3306/tcp, 33060/tcp                         friendly_proskuriakova
```

Observar els logs de la app i que ha connectat a la BD
```
$ docker logs nice_hawking 
yarn install v1.22.18
[1/4] Resolving packages...
warning Resolution field "ansi-regex@5.0.1" is incompatible with requested version "ansi-regex@^2.0.0"
warning Resolution field "ansi-regex@5.0.1" is incompatible with requested version "ansi-regex@^3.0.0"
success Already up-to-date.
Done in 0.34s.
yarn run v1.22.18
$ nodemon src/index.js
[nodemon] 2.0.13
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node src/index.js`
Waiting for mysql:3306.
Connected!
Connected to mysql db at host mysql
Listening on port 3000
```

Escriure ítems en la llista de la app
```
Visualitzar 127.0.0.1:3000
```

Accedir amb *docker exec* a l'interior del container mysql i observar que 
s'hi han afegit els ítems de la app (el passwd és *secret*).
```
docker exec -it <mysql-container-id> mysql -p todos

mysql> select * from todo_items;
+--------------------------------------+-----------+-----------+
| id                                   | name      | completed |
+--------------------------------------+-----------+-----------+
| d109e77f-ac27-4920-9140-4d078b4f55a8 | patata    |         0 |
| 4a760546-caa8-4c57-b389-9236214016df | pere      |         0 |
| df24c48b-a8ac-4c9d-8050-1b7d8049de2e | peix      |         0 |
| f035172a-e6ed-4013-b593-b6500a1e53c1 | pastanaga |         0 |
+--------------------------------------+-----------+-----------+
4 rows in set (0.00 sec)

mysql> exit

$ docker stop friendly_proskuriakova nice_hawking 
```


#### Pas-7: Use docker-compose


Desplegar la app amb mysql usant aquest docker-compose.yml file.

```
version: "3.7"

services:
  app:
    image: node:12-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 3000:3000
    working_dir: /app
    volumes:
      - ./:/app
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
      MYSQL_DB: todos

  mysql:
    image: mysql:5.7
    volumes:
      - todo-mysql-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: todos

volumes:
  todo-mysql-data:
```

```
$ docker-compose up -d
Creating network "app_default" with the default driver
Creating app_app_1   ... done
Creating app_mysql_1 ... done

$ docker-compose ps
   Name                  Command               State                    Ports                  
-----------------------------------------------------------------------------------------------
app_app_1     docker-entrypoint.sh sh -c ...   Up      0.0.0.0:3000->3000/tcp,:::3000->3000/tcp
app_mysql_1   docker-entrypoint.sh mysqld      Up      3306/tcp, 33060/tcp          

$ docker-compose down
```



