# getting started 
## @edt ASIX-M05 Curs 2021-2022


### Exemple de Getting Started 

Exemples de Docker Documentation

 1) Comptador de visites: compose / swarm
 2) Bulletin Board: compose / kubernetes / swarm
 3) Comptador de visites-2: compose amb volume i variable, edició interactiva.
 4) Getstarted Swarm: exemples amb *docker service* alpine (ping) i redis
 5) Items: aplicació web on anotar items.
 6) Tricks vàris (extrets del 4 Getstarted Swarm))

Altres exemples

 * Netxcloud
 * IsardVDI
 * Postgres
 * Grafana
 * asix-examples
 * http-ldap

---

### Docker Documentation


[Docker samples overview](https://docs.docker.com/samples/)


[Sample apps with Compose](https://docs.docker.com/compose/samples-for-compose)

 * *Quickstart: Compose and Django* - Shows how to use Docker Compose to set up and run a simple Django/PostgreSQL app.

 * *Quickstart: Compose and Rails* - Shows how to use Docker Compose to set up and run a Rails/PostgreSQL app.

 * *Quickstart: Compose and WordPress* - Shows how to use Docker Compose to set up and run WordPress in an isolated environment with Docker containers.

 * *Awesome-compose GitHub repo* -  The Awesome Compose samples provide a starting point on how to integrate different frameworks and technologies using Docker Compose.


[Example-voting-app](https://github.com/dockersamples/example-voting-app)


[Docker Official Images](https://docs.docker.com/docker-hub/official_images/) [Docker Hub](https://hub.docker.com/search?q=&type=image&image_filter=official)


