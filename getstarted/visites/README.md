# getting started - Comptador 
## @edt ASIX-M05 Curs 2021-2022


### Exemple de Getting Started - 1: Comptador de visites


Crear un servei web amb Flask amb un comptador de visites Redis. Mostra el nom
del host (en aquest cas el container) que es visita.

  * Pàgina web amb Flask
  * Comptador de visites Redis
  * Persistència del comptador amb volum


### Aplicat a:

 1) **Compose** Implementar amb docker-compose l'aplicació progressant pas a pas. 

      * *Pas-1*: Primerament implementar el servei Flask en Python que genera una pàgina web amb un
        comptador de visites que no funcionarà perquè no està implementat Redis.

      * *Pas-2*: Després implementar Redis per tneir el comptador de visites, però no tindrà 
        persistència de dades. Cada cop que es reinicialitzi el desplegament el comptador
        comença de nou.

      * *Pas-3*: Afegir persistència de dades al comptador Redis usant un volume.

      * *Pas-4*: Afegir un visualitzer (si no s'està a un swarm no es veu res d'interès).

      * *Pas-5*: Afegir un portainer per administrar tot el conjunt.

 2) **Swarm** Implemenar en un swarm el mateix exercici anterior, pas a pas o anant de dret
      a l'element final. Primerament cal iniciar el swarm, afegir els nodes que calgui (pot ser
      només un node leader) i fer el desplegament amb *docker stack*.  


 3) **Deploy** fer el deploy de l'aplicació modificant el número de rèpliques i altres
      característiques observant les opcions de *docker stack*, *docker service*, 
      *docker swarm* i *docker nodes*.



