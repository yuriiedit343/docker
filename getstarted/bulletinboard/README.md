# getting started - Bulletin Board
## @edt ASIX-M05 Curs 2021-2022


### Exemple de Getting Started - 2: Bulletin Board


Descarregar la app
```
$ git clone -b v1 https://github.com/docker-training/node-bulletin-board

$ cd node-bulletin-board/bulletin-board-app
```

Observar el dockerfile
```
$ cat Dockerfile 
FROM node:6.11.5

WORKDIR /usr/src/app
COPY package.json .
RUN npm install
COPY . .
CMD [ "npm", "start" ]
```

Generar la imatge
```
docker image build -t bboard:1.0 .
docker container run --publish 8000:8080 --detach --name bboard bboard:1.0
docker container rm --force bboard
```

Docker compose per a  stack
```
version: '3.7'    
services:
  bb-app:
	image: bboard:1.0
	ports:
  	- "8000:8080"
```

Desplegar la app i accedir-hi via localhost:8000 (127.0.0.1:8000)
```
docker swarm init
docker stack deploy -c bb-stack.yaml demo

docker service ls

docker stack rm demo
Docker swarm leave --force
```

**nota** La imatge està pujada alrepositori com a *edtasixm05/getstarted:bboard*, es
pot editar el .yml i usar directament aquesta imatge. També s'ha generat un fitxer
alternatiu m05bboard.yml.

```
$ docker stack deploy -c m05bboard.yml demo
Creating network demo_default
Creating service demo_bb-app

$ docker stack rm demo 
Removing service demo_bb-app
Removing network demo_default
```


