# Deploy
## @edt ASIX-M05 Curs 2021-2022

## Deploy amb  Stack / Service / Swarm

Desplegar amb *docker stack* en un swarm d'un o més nodes l'aplicació 
d'exemple getstarted comptador tot modificant les condicions del deploy.

  * Stack
  * Service
  * Swarm

---

### Docker Stack

Per desplegar un stack cal primerament iniciar un swarm en un o més nodes.
En aquest exemple es desplegarà la pàgina web del comptador de visites de
l'exemple *getstarted comptador*.

Conceptes/Passos:
 * Iniciar el swarm
 * Desplegar l'stack i observar les diferents ordres de stack
 * Modificar el deploy

#### Iniciar el swarm

```
$ docker swarm init
Swarm initialized: current node (wgrewlz8t4yx0tznc9ru0dq23) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-5zhhg5u5xf1yvihqwsrq4jd67smmptakyr9dlbhl03g0nefv9j-5lklxx77v427haraii4t8iruw 192.168.1.45:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

#### Desplegar l'stack

```
$ docker stack 
deploy    ls        ps        rm        services  
```

```
$ docker stack deploy -c docker-compose.yml  myapp
Creating network myapp_webnet
Creating service myapp_portainer
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer
```

```
$ docker stack ps 
"docker stack ps" requires exactly 1 argument.
```

```
$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
2ka7r9j425nr   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
1r819z9m5076   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
v6b3dn0flx96   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
pj4zsnjtkqac   myapp_web          replicated   3/3        edtasixm05/getstarted:comptador   *:80->80/tcp
```

```
$ docker stack ps myapp
ID             NAME                 IMAGE                             NODE               DESIRED STATE   CURRENT STATE                ERROR     PORTS
zakcp5p73i5h   myapp_portainer.1    portainer/portainer:latest        mylaptop.edt.org   Running         Running about a minute ago             
w0a6juuycvsk   myapp_redis.1        redis:latest                      mylaptop.edt.org   Running         Running about a minute ago             
ktdf2hafd52a   myapp_visualizer.1   dockersamples/visualizer:stable   mylaptop.edt.org   Running         Running about a minute ago             
hqpnwjenkqsj   myapp_web.1          edtasixm05/getstarted:comptador   mylaptop.edt.org   Running         Running about a minute ago             
ulifz8yc62s8   myapp_web.2          edtasixm05/getstarted:comptador   mylaptop.edt.org   Running         Running about a minute ago             
v05shzjawapt   myapp_web.3          edtasixm05/getstarted:comptador   mylaptop.edt.org   Running         Running about a minute ago             
```

```
$ docker stack rm myapp 
Removing service myapp_portainer
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```

#### Modificar el deploy

Quan es modifica el fitxer docker-compose que descriu quin és l'estat que
es vol tenir en el desplegament simplement fent de nou el *deploy* docker
és capaç de recalcular i reorganitzar els canvis per passar al *desired state*.

```
$ docker stack deploy -c docker-compose.yml  myapp
Creating network myapp_webnet
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_portainer

$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
k0tt9mk3c7yr   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
gu5aihtu1isi   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
tzb6m1vhesp5   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
f56emr13car4   myapp_web          replicated   3/3        edtasixm05/getstarted:comptador   *:80->80/tcp
```

**Pasar de 3 rèpliques web a 5 rèpliques**

```
# Modificar el fitxer docke-compose.yml i establir al servei web 5 repliques
```

```
$ docker stack deploy -c docker-compose.yml  myapp
Updating service myapp_web (id: f56emr13car4hs8k8p68kplwr)
Updating service myapp_redis (id: gu5aihtu1isiqa6d3xbjscldk)
Updating service myapp_visualizer (id: tzb6m1vhesp5s2lmcyu885i62)
Updating service myapp_portainer (id: k0tt9mk3c7yrbhd83sqkc965o)

$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
k0tt9mk3c7yr   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
gu5aihtu1isi   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
tzb6m1vhesp5   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
f56emr13car4   myapp_web          replicated   5/5        edtasixm05/getstarted:comptador   *:80->80/tcp
```

**Passar de 5 rèpliques web a 2 rèpliques**

```
Modificar el fitxer docker-compose.yml i posar al servei web 2 rèpliques
```

```
$ docker stack deploy -c docker-compose.yml  myapp
Updating service myapp_portainer (id: k0tt9mk3c7yrbhd83sqkc965o)
Updating service myapp_web (id: f56emr13car4hs8k8p68kplwr)
Updating service myapp_redis (id: gu5aihtu1isiqa6d3xbjscldk)
Updating service myapp_visualizer (id: tzb6m1vhesp5s2lmcyu885i62)

$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
k0tt9mk3c7yr   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
gu5aihtu1isi   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
tzb6m1vhesp5   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
f56emr13car4   myapp_web          replicated   5/2        edtasixm05/getstarted:comptador   *:80->80/tcp
```

---

### Docker Service

Es poden gestionar els serveis amb l'ordre *docker service* tant si s'han desplegat amb
*docker-compose* com amb *docker swarm*. Les accions principals que permeten són:

 * ls
 * ps
 * scale
 * update
 * rm
 * logs
 * create
 * inspect


```
$ docker service 
create    inspect   logs      ls        ps        rm        rollback  scale     update    
```

```
$ docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
k0tt9mk3c7yr   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
gu5aihtu1isi   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
tzb6m1vhesp5   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
f56emr13car4   myapp_web          replicated   2/2        edtasixm05/getstarted:comptador   *:80->80/tcp
```

```
$ docker service ps myapp_web 
ID             NAME          IMAGE                             NODE               DESIRED STATE   CURRENT STATE            ERROR     PORTS
tzwnlwa2ih3p   myapp_web.1   edtasixm05/getstarted:comptador   mylaptop.edt.org   Running         Running 11 minutes ago             
pqinzne48crl   myapp_web.2   edtasixm05/getstarted:comptador   mylaptop.edt.org   Running         Running 11 minutes ago             

$ docker service ps myapp_visualizer 
ID             NAME                 IMAGE                             NODE               DESIRED STATE   CURRENT STATE            ERROR     PORTS
sqiofz5yh6ry   myapp_visualizer.1   dockersamples/visualizer:stable   mylaptop.edt.org   Running         Running 11 minutes ago             
```

```
$ docker service logs myapp_redis 
myapp_redis.1.xkx9k9ldbzdw@mylaptop.edt.org    | 1:C 31 May 2022 20:21:45.225 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
myapp_redis.1.xkx9k9ldbzdw@mylaptop.edt.org    | 1:C 31 May 2022 20:21:45.225 # Redis version=7.0.0, bits=64, commit=00000000, modified=0, pid=1, just started
...
```

```
$ docker service rm myapp_portainer 
myapp_portainer
$ docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
gu5aihtu1isi   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
tzb6m1vhesp5   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
f56emr13car4   myapp_web          replicated   2/2        edtasixm05/getstarted:comptador   *:80->80/tcp
```

**scale services**

```
$ docker service scale myapp_web=4
myapp_web scaled to 4
overall progress: 4 out of 4 tasks 
1/4: running   [==================================================>] 
2/4: running   [==================================================>] 
3/4: running   [==================================================>] 
4/4: running   [==================================================>] 
verify: Service converged 

$ docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
gu5aihtu1isi   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
tzb6m1vhesp5   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
f56emr13car4   myapp_web          replicated   4/4        edtasixm05/getstarted:comptador   *:80->80/tcp
```

```
$ docker service scale myapp_web=2
myapp_web scaled to 2
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 

$ docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
gu5aihtu1isi   myapp_redis        replicated   1/1        redis:latest                      \*:6379->6379/tcp
tzb6m1vhesp5   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   \*:8080->8080/tcp
f56emr13car4   myapp_web          replicated   4/2        edtasixm05/getstarted:comptador   \*:80->80/tcp

$ docker service ps myapp_web 
ID             NAME          IMAGE                             NODE               DESIRED STATE   CURRENT STATE            ERROR     PORTS
tzwnlwa2ih3p   myapp_web.1   edtasixm05/getstarted:comptador   mylaptop.edt.org   Running         Running 16 minutes ago             
pqinzne48crl   myapp_web.2   edtasixm05/getstarted:comptador   mylaptop.edt.org   Running         Running 16 minutes ago     
```

```
$ docker stack rm myapp 
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```

---

### Docker swarm

Tal i com s'ha vist es poden posar un o més nodes en un swarm per crear
un cluster de hosts que treballen onjutament. A l'inici de tot
s'ha vost com crear un swarn en el node *leader*. La mateixa instrucció
indica què cal fer en els nodes *worker* per unir-se al swarm.

Per gestionar noes i swarms convé tractar les ordres:
 
 * docker swarm

 * docker node

```
$ docker swarm 
ca          init        join        join-token  leave       unlock      unlock-key  update      
```

```
$ docker node 
demote   inspect  ls       promote  ps       rm       update   
```

Apagar el swarm iniciat en aquest exercici
```
$ docker swarm leave --force
Node left the swarm.
```

