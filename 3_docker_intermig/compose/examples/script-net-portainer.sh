#! /bin/bash
# @edt ASIX-M05 Curs 2021-2022
#
# Engegar els conatiners net21:base i portainer

docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 80:80 -d edtasixm05/net21:base
docker run --rm -p 9000:9000 --net mynet -v /var/run/docker.sock:/var/run/docker.sock -d portainer/portainer
docker ps
