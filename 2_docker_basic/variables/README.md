# docker
## @edt ASIX-M05 Curs 2021-2022

# Variables 

Es poden passar variables d’entorn del sistema operatiu als containers a través de l’opció -e de l’ordre docker run. Aquest és un mecanisme molt usat per passar options de configuració que defineixen com s’ha de crear el container.

Les variables d’entorn que es passen es poden definir en la propia ordre docker run o bé poden ser variables ja existents però que han de formar pert de l’entron, han d’estar exportades amb set.

  * S’ha definit una variable curs i s’ha exportat a l’entorn.
  * S’ha definit una variable aula i posteriorment s’ha exportat a l’entorn.
  * S’ha definit una variable cicle però no s’ha exportat.
  * En l’ordre docker run s’ha definit explícitatment la variable nom al valor pere.
  * En l’ordre docker run observem que les variables curs, aula i nom tenen valors definits, però no la variable cicle (perquè no estava exportada)
  * Observem que si amb -e exportem variables podem només posar el nom si estan exportades. Podem posar nom=valor i es defineixen en aquest mateix moment per al container.

```
$ export curs="docker"

$ aula="n2j"

$ export aula

$ cicle="2hisix"

$ docker run --rm -e curs -e aula -e cicle -e nom=pere debian env
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=1fcca7cce4bd
curs=docker
aula=n2j
nom=pere
HOME=/root

$ docker run --rm -e curs -e aula -e cicle -e nom=pere -it debian 
root@ed14acd438d8:/# echo $curs $aula $cicle $nom
docker n2j pere
root@ed14acd438d8:/# exit
exit
```

### Exemple d’utilització de variables amb Postgres

La imatge oficial al DockrHub de Postgres, postgres oficial, permet definir el comportament de la creació del container segons indiquen una gran quantitat de variables. 
Repositori i documentació de la imatge Docker oficial de postgres:
[Postgres oficial image](https://hub.docker.com/_/postgres)

Observem que a l’apartat **How To Extend This Image** hi ha una secció anomenada **Environment Variables** on es defineixen les variables:
  * POSTGRES_PASSWORD
  * POSTGRES_USER
  * POSTGRES_DB
  * POSTGRES_INITDB_ARGS
  * POSTGRES_INITDB_WALDIR
  * POSTGRES_HOST_AUTH_METHOD
  * PGDATA

Així per exemple podem personalitzar la creació del container amb:

```
$ docker run -d \
	--name some-postgres \
       -e POSTGRE_USER=postgres \
       -e POSTGRES_DB=training \
	-e POSTGRES_PASSWORD=mysecretpassword \
	-e PGDATA=/var/lib/postgresql/data/pgdata \
	-v /custom/mount:/var/lib/postgresql/data \
	postgres
```


