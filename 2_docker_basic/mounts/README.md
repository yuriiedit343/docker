 docker
## @edt ASIX-M05 Curs 2021-2022

# Persistència de dades: bind mounts / volums


Un dels problemes que es presenta en la utilització de containers és la persistència de dades. En els containers que hem estat fent fins ara les dades del container es perden en eliminar el container, això pot ser bó si volem que el container sempre tingui les mateixes dades originals, però si volm que les dades es puguin anar modificant entre execucions dle container ja no és tant bò.

Imaginem un container amb una base de dades postgres, les dades s’aniran modificant al llarg de la vida de la base de dades i interessa que perdurin encara que el container finalitzi o entre diferents execucions del container.

Podem consultar la documentació de Docker Documentation corresponent  les dues tècniques utilitzades per proporcionar persistència de dades:

 * Bind Mounts [Use bind mounts](https://docs.docker.com/storage/bind-mounts/)

 * Volumes [Use volumes](https://docs.docker.com/storage/volumes/)

Un altre dels beneficis d’usar aquestes tecnologies es poder combinar les dades localment al host amfitrió i al mateix temps usar-les dins el container. Això permet per exemple editar localment una web (en el host amfitrió)que el container està publicant i anar observant els canvis en calent. O per exemple corregir exercicis locals de scripts dels alumnes (que s’executen com a root) en un container GNU/Linux de manera segura., els exercicis estan al host amfitrió però s’executen en el container.



## Usar bind mounts

Bind mounts és una tècnica similar als mounts de GNU/Linux que permet muntar dins del container contingut que en realitat està en el host amfitrió. Aquest contingut pot ser un sol fitxer o un directori.

Anem a usar la imatge amb el servidor web per practicar com modificar en calent la web, el contingut i el format, editant els fitxers html o els css i observant que el container reflecteix els canvis a l’instant.

La sintaxis bàsica per realitzar bind mounts és el paràmetre -v de l’ordre docker run:
```
docker run -v file-or-directory-host:file-or-directory-container ...
```

**Atenció**: la ruta de l’element extern (la primera part) ha de ser sempre una ruta absoluta, no pot ser una ruta relativa. A vegades es fa servir command substitution de GNU/Linux per indicar el directori actiu com a **$(pwd)**.

Existeix una altra sintàxi més descriptiva, però en general treballarem amb l’opció **-v** que és més simple.


### Bind mount d’un fitxer

Primerament prepararem l’entorn de treball. Es creraà un directori per practicar aquest exemple a /tmp/web i dins s’hi posarà aquell contingut que ha de tenir el servidor web. Es desplegarà el container i es tracta d’observar que modificant els fitxers del host amfitrió els canvis es reflecteixen en la seu web.

  * Crear el directori de desenvolupament web i posar-hi els fitxers web.

  * En el nostre cas serem molt simplistes i farem una simple pàgina web index.html.

```
$ mkdir /tmp/web

$ cd /tmp/web/

$ echo "pagina web en desenvolupament" > index.html
```

  * Engegar el servidor web fent un bind mount del fitxer extern (del host amfitrió) /tmp/web/index.html al fitxer dins el container, en la ruta de publicació /var/www/html/index.html.

  * És a dir, estem dient que el fitxer index.html de dins del container és en realitat el que hi ha fora.

```
$ docker run --rm --name web.edt.org -v /tmp/web/index.html:/var/www/html/index.html -p 80:80 -d edtasixm05/myweb
504fc81e80c6ca8fa1c8c117e04b5f08a843dec946e3793315093a93de375bb4

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                               NAMES
504fc81e80c6   edtasixm05/myweb   "/bin/sh -c 'apachec…"   3 seconds ago   Up 2 seconds   0.0.0.0:80->80/tcp, :::80->80/tcp   web.edt.org
```

  * Verifiquem que el servidor web mostra la pàgina web de desenvolupament.

```
$ telnet localhost 80
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Wed, 06 Jul 2022 16:32:53 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Wed, 06 Jul 2022 16:23:42 GMT
ETag: "1e-5e3256475aedf"
Accept-Ranges: bytes
Content-Length: 30
Connection: close
Content-Type: text/html

pagina web en desenvolupament
Connection closed by foreign host.
```

![image1](images/image1.png)


  * Anem a modificar des del host amfitrió el fitxer index.html. Important, useu l’editor vim o un editor de consola, no un editor gràfic.

```
$ pwd
/tmp/web

$ vim index.html 

$ cat index.html 
pagina web en desenvolupament, ara modificada
quan en sapiga li posaré CSS
```

  * I comprovem que en calent la web ha canviat. Només cal recarregar la pàgina. Ull, a vegades els navegadors fan trampetes…


![image2](images/image2.png)


  * Aturem el servidor web.

```
$ docker stop web.edt.org 
```


### Bind mount d’un directori

Anem a repetir el mateix exemple però ara fent el lligam entre el directori extern del host amfitrió /tmp/web al directori intern del container de publicació /var/www/html. Tot allò que hi ha localment al directori /tmp/web el container ho veu com si fos el seu directori /var/www/html.

 * Primerament creem un altre fitxer html dins el directori de desenvolupament /tmp/web. L’anomenarem abudhabi.html

```
$ pwd
/tmp/web

$ echo "aquest és un lloc molt bonic on dictadors de tot el mon poden compartir experiencies" > abudhabi.html

$ ls
abudhabi.html  index.html
```

  * Engegem el servei web fent el bind mount del directori local de desenvolupament al directori intern de publicació web.

```
$ docker run --rm --name web.edt.org -v /tmp/web:/var/www/html -p 80:80 -d edtasixm05/myweb
0ed382b64ee21e41eba60cf3968f20401c261d92daf655ce3543baccb91f779d

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                               NAMES
0ed382b64ee2   edtasixm05/myweb   "/bin/sh -c 'apachec…"   2 seconds ago   Up 2 seconds   0.0.0.0:80->80/tcp, :::80->80/tcp   web.edt.org
```

Verifiquem el servei web i que podem accedir als dos fitxers.

![image3](images/image3.png)
```
```
![image4](images/image4.png)
```
```

  * Ens han manat modificar el contingut web perquè no fereixi sensibilitats de manera que editem el fitxer abudhabi.html.

```
$ pwd
/tmp/web

$ vim abudhabi.html 

$ cat abudhabi.html 
aquest es un lloc molt bonic on <b>constitucionalistes</b> de tot el mon poden compartir experiencies
```

  * I verifiquem que en calent el servei web publica la nova pàgina, simplement refrescant el navegador.

![image5](images/image5.png)


 * Anem a mirar dins del container això del bind mount com s’ha fet. Per fer-ho farem un docker exec i mirament amb l’ordre GNU/Linux mount què hi ha muntat.

```
$ docker exec -it web.edt.org /bin/bash

root@0ed382b64ee2:/tmp# mount
tmpfs on /var/www/html type tmpfs (rw,nosuid,nodev,seclabel,inode64)
…

root@0ed382b64ee2:/tmp# exit
exit
```

```
$ docker exec -it web.edt.org mount -t tmpfs | grep /var/www/html
tmpfs on /var/www/html type tmpfs (rw,nosuid,nodev,seclabel,inode64)
```

  * Aturem el servidor web.

```
$ docker stop web.edt.org 
```


### Exemple de corregir exercicis scripts d’alumnes

En aquest exemple veurem una aplicació pràctica de la utilització de bind mount. Suposem un directori on alumnes de sistemes operatius han lliurat scripts que s’han de provar executant-se com a root. Evidentment és molt perillós fer-ho en el propi sistema amfitrió de manera que es crerà un container GNU/Linux amb un bind mount del directori dels scripts.Així els scripts continuen estan localment però els podem provar de manrea segura dins el container.

  * Preparem l’entorn de prova. Per fer-ho ens baixarem els apunts d’aquest curs del GitLab i anirem al directori on hi ha la simulació preparada. Atenció: pot ser que el directori es trobi en un lloc diferent si s’ha modificat el Git.

```
$ cd /tmp/

$ git clone https://www.gitlab.com/edtasixm05/docker.git
Cloning into 'docker'...
warning: redirecting to https://gitlab.com/edtasixm05/docker.git/
remote: Enumerating objects: 640, done.
remote: Counting objects: 100% (462/462), done.
remote: Compressing objects: 100% (448/448), done.
remote: Total 640 (delta 209), reused 0 (delta 0), pack-reused 178
Receiving objects: 100% (640/640), 3.64 MiB | 6.48 MiB/s, done.
Resolving deltas: 100% (285/285), done.

$ cd docker/mounts/scripts/

$ ls
anna.sh  marta.sh  pere.sh
```

  * Engegem un GNU/Linux i li passem el directori actiu que és on hi ha els scripts a provar.

  * Observeu que el directori interior en el container es crea si no existeix.

```
$ docker run --rm -v $(pwd):/exercicis -it debian 

root@2b30a677bad8:/# cd /exercicis/

root@2b30a677bad8:/exercicis# ls
anna.sh  marta.sh  pere.sh

root@2b30a677bad8:/exercicis# bash pere.sh 
aquest script fa el que toca i ben fet

root@2b30a677bad8:/exercicis# bash marta.sh 
aquest script fa el que toca molt ben fet,
però posa una porta falsa per poder entrar el sistema quan vulgui

root@2b30a677bad8:/exercicis# bash anna.sh  
aquest script destrossa el disc dur

root@2b30a677bad8:/exercicis# exit
exit
```

### Exemple de corregir programes python d’alumnes

Anem a fer un exemple similar a l’anterior on ara corregim programes de python lliurats per alumnes. Aquests programes creen processos i serveis i s’han d’executar com a root en el sistema, i per tant són perillosos.

Els executarem en un entorn segur dins un container de python:3 amb el directori dels programes muntat. 

  * Podem seguir els passos anteriors per descarregar la documentació. Un cop descarregada anar al directori amb els programes de python.

```
$ cd ../python

$ pwd
/tmp/docker/mounts/python

$ ls
anna.py  marta.py  pere.py
```

  * Executar el container python:3 muntant el directori local dels programes dels alumnes dins el container en un directori a l’arrel que anomenem progarmes.

```
$ docker run --rm -v $(pwd):/programes -it python:3 /bin/bash 

root@15f49d03a600:/# cd /programes/

root@15f49d03a600:/programes# ls
anna.py  marta.py  pere.py

root@15f49d03a600:/programes# python pere.py 
aquest programa fa el que toca i ben fet

root@15f49d03a600:/programes# python marta.py 
aquest programa fa el que toca molt ben fet,
però posa una porta falsa per poder entrar el sistema quan vulgui

root@15f49d03a600:/programes# python anna.py 
aquest programa destrossa el disc dur

root@15f49d03a600:/programes# exit
exit
```


## Use volumes

Un mecanisme totalment diferent per proporcionar persistència de dades és la utilització de volums. Els volums són espais d’emmagatzemament que genera Docker que s’identifiquen amb un nom. Els podem imaginar com un USB que Docker guarda en algun lloc i que nosaltres utilitzem per el seu nom.

Els volums són un tipus d’objecte de Docker (com els containers o les imatges) i es gestionen amb la seva propia ordre docker volume. Podem fer les accions:

  * Crear-los
  * Llistar-los
  * Mirar-ne la definició
  * Eliminar-los
  * Prune (eliminar els que no s’utilitzen)

```
$ docker volume 
create   inspect  ls       prune    rm       
```

### Utilitzar l’ordre docker volume

Anem a veure els exemples d’utilització de l’ordre docker volume de gestió de volums.

```
$ docker volume create volum1
volum1

$ docker volume create volum2
volum2

$ docker volume ls
DRIVER    VOLUME NAME
local     volum1
local     volum2

$ docker volume rm volum2
volum2
 
$ docker inspect volum1 
[
    {
        "CreatedAt": "2022-07-06T19:25:48+02:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/volum1/_data",
        "Name": "volum1",
        "Options": {},
        "Scope": "local"
    }
]

$ docker volume prune 
WARNING! This will remove all local volumes not used by at least one container.
Are you sure you want to continue? [y/N] y
Deleted Volumes:
volum1
```


### Creació d’un volum i l’omplim amb dades persistents

En aquest exemple (ben senzill) crearem un volum i l’assignarem a un sistema GNULinux, li posarem dades i veurem que tot i eliminar el container les dades perduren. Es poden tornar assignar a un altre container.


  * Crear el volum i fer l’inspect

```
$ docker volume create mydata
mydata

$ docker volume inspect mydata 
[
    {
        "CreatedAt": "2022-07-06T19:29:46+02:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/mydata/_data",
        "Name": "mydata",
        "Options": {},
        "Scope": "local"
    }
]
```

  * Ara tenim un espai d’emmagatzemament anomenat mydata que podem associar a containers.

  * Creem un container GNU/Linux i associem al volum a un directori que es crearà nomenat /dades. Creem dins d’aquest directori tres fitxesr i pleguem. El container s’elimina.

```
$ docker run --rm -v mydata:/dades -it debian 

root@32834f6906aa:/# cd /dades

root@32834f6906aa:/dades# touch file1 file2 file3

root@32834f6906aa:/dades# ls 
file1  file2  file3

root@32834f6906aa:/dades# exit
exit
```

  * Observem que el volum continua mantenint les dades. Ara generem un nou container Fedora (i no debian) i posem les dades del volum en un directori a /tmp anomenat fitxers.

```
$ docker run --rm -v mydata:/tmp/fitxers -it fedora:32 /bin/bash

[root@d386a583e976 /]# cd /tmp/fitxers/

[root@d386a583e976 fitxers]# ls
file1  file2  file3

[root@d386a583e976 fitxers]# exit
exit
```

  * On són les dades realment? Com usuaris ens és igual, sabem que Docker les té guardades en un volum amb el nom mydata i que perduren i les podem usar quan calgui. 

  * Físicament en fer l’ordre docker inspect hem observat que estan en el directori /var/lib/docker/volumes. Per a cada volum hi ha un directori amb metadades i les dades propiemant dites en el directori _data.

```
$ sudo tree /var/lib/docker/volumes/
/var/lib/docker/volumes/
├── backingFsBlockDev
├── metadata.db
└── mydata
    └── _data
        ├── file1
        ├── file2
        └── file3
```

  * Eliminar el volum si no l’hem d’utilitzar.

```
$ docker volume rm mydata 
mydata
```

### Usar postgres (la nostra versió) i una base de dades persistent

Anem a veure un exemple més complert d’utilització d’un volum com a mecanisme de persistència de dades d’una base de dades. Usarem un container Postres amb la base de dades d’exemple de l’escola Training. 

Primerament observarem que si no usem volums tots els canvis a la base de dades es perden si s’elimina el container. Si es crea un nou container aquest torna a tenir totes les dades originals.

Després crearem un volum on contenir les dades de la base de dades, associant-lo al directori de dades de Postgres. D’aquesta manera veurem que si fem actualitzacions a les dades encara que el container s’elimini les dades actualitzades perduren. En crear de nou un container usant el volum els canvis es mantenen.

  * Engegar un container postgres amb la base de dades training. S’utilitza una imatge ja preparada que incorpora postgres i training.

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d edtasixm05/getstarted:postgres 
3d7a38d668ba5dd3856291ce785943e58db93e8520327e51633eef93de0bc2d6

$ docker ps
CONTAINER ID   IMAGE                            COMMAND                  CREATED         STATUS         PORTS      NAMES
3d7a38d668ba   edtasixm05/getstarted:postgres   "docker-entrypoint.s…"   2 seconds ago   Up 2 seconds   5432/tcp   training
```

  * Verificar les taules i llistar algun registre. En aquest exemple ho fem generant un altre container i executant l’interpret psql que connecta amb el servidor postgres (el servidor ha de ser l’adreça IP indicada). El password és passwd.

```
$ docker run -it --rm  postgres psql -h 172.17.0.2 -U postgres -d training
Password for user postgres: passwd
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)

training=# select * from oficinas;
 oficina |   ciudad    | region | dir | objetivo  |  ventas   
---------+-------------+--------+-----+-----------+-----------
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00
      11 | New York    | Este   | 106 | 575000.00 | 692637.00
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00
(5 rows)

training=# \q
```

  * Una altra manera de verificar les dades del servidor Postgres és entrant-hi directament amb docker exec i realitzar una sessió de l’interpret SQL psql, o directament exeutar-lo. En ser local al servidor no cal password.

```
$ docker exec -it training  psql -d training -U postgres
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)

training=# select * from repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
(10 rows)

training=# \q
```

  * Anem a fer modificacions a l’engròs. Ens carreguem la taula oficinas.

```
$ docker exec -it training  psql -d training -U postgres
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# drop table oficinas;
DROP TABLE

training=# select * from oficinas;
ERROR:  relation "oficinas" does not exist
LINE 1: select * from oficinas;
                      ^
training=# \q
```

  * Si tanquem el servidor i l’engeguem de nou veiem que la taula oficinas torna a ser-hi, no hi ha persistència de les dades als canvis.

```
$ docker stop training 
Training

$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d edtasixm05/getstarted:postgres 
a66b08656988cf6ec297eafa07c00039c289e42c044a7442a62536529a24e977

$ docker exec -it training  psql -d training -U postgres \d
psql: warning: extra command-line argument "d" ignored
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)

training=# select * from oficinas;
 oficina |   ciudad    | region | dir | objetivo  |  ventas   
---------+-------------+--------+-----+-----------+-----------
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00
      11 | New York    | Este   | 106 | 575000.00 | 692637.00
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00
(5 rows)

training=# \q
```

```
$ docker stop training 
training
```

**Persistència**

Anem a fer que les dades de Postgres de la base de dades Training no resideixin en el container sinó en un volum anomenat postgres-data.  En la imatge utilitzada Postgres desa les dades de les bases de dades en el directori **/var/lib/postgresql/data**, però el directoris de dades depèn de la distribució de GNU/Linux utilitzada.

  * Primerament creem el volum.

  * Després engeguem Postgres associant el volum al directori de dades de Postgres segons el sistema operatiu que s’estigui utilitzant en el container.

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -v postgres-data:/var/lib/postgresql/data -d edtasixm05/getstarted:postgres 
1e07bd7282c5e0fa93863fad7c485da4bbef5289480c023d7d5e0c2fd6fe455a

$ docker ps
CONTAINER ID   IMAGE                            COMMAND                  CREATED         STATUS         PORTS      NAMES
1e07bd7282c5   edtasixm05/getstarted:postgres   "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   5432/tcp   training
```

  * Podem observar que en crear-se el container el populate de dades s’ha fet dins del volum postgres-data.

```
$ sudo tree /var/lib/docker/volumes/postgres-data/ | head
/var/lib/docker/volumes/postgres-data/
└── _data
    ├── base
    │   ├── 1
    │   │   ├── 112
    │   │   ├── 113
    │   │   ├── 1247
    │   │   ├── 1247_fsm
    │   │   ├── 1247_vm
    │   │   ├── 1249
```

  * Ara ens carreguem la taula oficinas.

```
$ docker exec -it training  psql -d training -U postgres 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# drop table oficinas;
DROP TABLE

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(4 rows)

training=# \q
```

  * Aturem el servei postgres de manera que el container s’eliminarà. L’engeguem de nou en un nou container usant el volum de dades i observem que ara no existeix la taula oficinas.

```
$ docker stop training 
training

$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -v postgres-data:/var/lib/postgresql/data -d edtasixm05/getstarted:postgres 
f022424e73c1ecd9474cae287c39d544de0187fa3860dfd630e2b375e3d1d014

$ docker exec -it training  psql -d training -U postgres 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(4 rows)

training=# \q
```

  * Aturem el servidor postgres.

```
$ docker stop training 
training
```


### Usar la imatge postgres original amb populate i persistència de dades

En aquesta pràctica veurem com usar la imatge original de Postgres que proporcionen els propis creadors. Conté un mecanisme per poder-la inicialitzar amb un contingut concret, fer el **populate** de dades, usant scripts de linux o de SQL. I un cop creada la base de dades com proporcionar persistència a les dades usant un volum.

Els creadors de la imatge de Postgres proporcionen un mecanisme automàtic per crear un container amb dades ja incorpprades. En crear el container tot allò que hi hagi en el directori **/docker-entrypoint-initdb.d** s’executarà per ordre alfabètic, siguin scripts de shell o de SQL. El truc consisteix en preparar en un directori les dades i scripts d’incialització i fer un **bind mount** d’aquest directori amb el directori /docker-entrypoint-initdb.d. D’aquesta manera en crear-se el container els executa i omple la Base de Dades.

Si a més a més volem tenir persistència de dades podem crear un **volum** i associar-lo al directori de dades de Postgres. Aquest directori depèn de la distribució, però en aquest cas és el directori **/var/lib/postgresql/data**. 

  * Crear el volum de dades si no existeix.

```
$ docker volume create postgres-data
Postgres-data

$ docker volume ls
DRIVER    VOLUME NAME
local     postgres-data
```

  * Crear un directori de desenvolupament on tenir els fitxers script i dades amb els que volem fer el populate de la base de dades. En el nostre cas utilitzarem un directori ja preparat dels apunts de GitLab de docker i ens situarem en aquest directori com a directori actiu per engegar el servei.

  * Si no el tenim ja descarregat descarregar del Git el material del curs i anar al directori pertinent. Atenció! Els directoris poden canviar de lloc.

```
$ pwd
/tmp

$ git clone https://www.gitlab.com/edtasixm05/docker.git
Cloning into 'docker'...
warning: redirecting to https://gitlab.com/edtasixm05/docker.git/
remote: Enumerating objects: 675, done.
remote: Counting objects: 100% (497/497), done.
remote: Compressing objects: 100% (481/481), done.
remote: Total 675 (delta 214), reused 0 (delta 0), pack-reused 178
Receiving objects: 100% (675/675), 3.65 MiB | 5.59 MiB/s, done.
Resolving deltas: 100% (290/290), done.

$ cd docker/mounts/postgres/training/

$ pwd
/tmp/docker/mounts/postgres/training

$ ls
clientes.dat  instalar_postgres.txt  oficinas.sql  pedidos.sql    productos.sql  repventas.sql
clientes.sql  oficinas.dat           pedidos.dat   productos.dat  repventas.dat
```

  * Engegar el servei postgres indicant el bind mount del directori de inicialització i el volum de dades on desar la base de dades training. També s’indica el nom de la base de dades i el password a usar.
    * **–name training** el nom del container
    * **-e POSTGRES_PASSWORD=passwd** defineix el nom del password per connectar com usuari postgres.
    * **-e POSTGRES_DB=training** defineix el nom de la base de dades a la que connectar o crear.
    * **-v $(pwd):/docker-entrypoint-initdb.d** indica que cal fer el bind mount del directori actiu (on hi ha els scripts i les dades d’inicialització) al directori preparat per Postgres per contenir la inicialització.
    * **-v postgres-data:/var/lib/postgresql/data** indica el volum de dades per tenir la persistència de les dades de postgres. Es munta sobre el directori on postgres desa totes les bases de dades.
    * **-d postgres** imatge postgres oficial a engegar en detach.

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd):/docker-entrypoint-initdb.d  -v postgres-data:/var/lib/postgresql/data -d postgres
cac7f74b3cd6a4e210614ffd215077f7d33907c1fdf93116caa67b5521024417

$ docker ps
CONTAINER ID   IMAGE      COMMAND                  CREATED         STATUS         PORTS      NAMES
cac7f74b3cd6   postgres   "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   5432/tcp   training
```

```
$ docker exec -it training  psql -d training -U postgres 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(4 rows)

training=# \q
```

  * Aturar el container i eliminar els volums si no els usem

```
$ docker stop training 
training

$ docker volume prune
```

### Exemples de SQL injectat usant volumes

Anem a usr el container de postgres amb la base de dades training per practicar exemples de SQL injectat, especialment exemples nocius. Per fer-ho usarem un directory local on tenim preparats scripts de python que ataquen la base de dades Postgres Training. Els incorporarem al container usant un bind mount del directori dels exercicis dins el container.

  * Primerament ens siturem en el directori dels apunts on hi ha els exercicis d’exemple. Si no ho hem fet abans podem descarregar els apunts del GitLab. Atnció que la ubicació dels directoris pot canviar.

```
$ cd /tmp/

$ git clone https://www.gitlab.com/edtasixm05/docker.git
Cloning into 'docker'...
warning: redirecting to https://gitlab.com/edtasixm05/docker.git/
remote: Enumerating objects: 640, done.
remote: Counting objects: 100% (462/462), done.
remote: Compressing objects: 100% (448/448), done.
remote: Total 640 (delta 209), reused 0 (delta 0), pack-reused 178
Receiving objects: 100% (640/640), 3.64 MiB | 6.48 MiB/s, done.
Resolving deltas: 100% (285/285), done.

$ cd mounts/postgres/sql-injection/

$ pwd
/tmp/docker/mounts/postgres/sql-injection

$ ls
12-popen-sql.py  13-popen-sql-injectat.py  13-sql-injectat.py  14-popen-sql-multi.py  README.md
```

  * Engegar el servei postgres en la xarxa per defecte.

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d edtasixm05/getstarted:postgres
d57c366ad7b0075579e22555e1b520b06870da3756c8ca23f91b22f70caf003b
```

  * Engegar un client psql interactu que comunica amb el servei postgres anterior, és una imatge ja preparada edtasixm05/postgres21:client-psql-py amb Python i el client psql de Postgres (el password de postgres és passwd). Munta el directori sql-injection amb els exemples de python a provar al directori /tmp per poder practicar.

```
$ docker run  --rm -v $(pwd):/tmp -it  edtasixm05/postgres21:client-psql-py

/ # python /tmp/12-popen-sql.py 
Password for user postgres: 
2111,JCP Inc.,103,50000.00
2102,First Corp.,101,65000.00
2103,Acme Mfg.,105,50000.00
2123,Carter & Sons,102,40000.00
2107,Ace International,110,35000.00
2115,Smithson Corp.,101,20000.00
2101,Jones Mfg.,106,65000.00
2112,Zetacorp,108,50000.00
2121,QMA Assoc.,103,45000.00
2114,Orion Corp,102,20000.00
2124,Peter Brothers,107,40000.00
2108,Holm & Landis,109,55000.00
2117,J.P. Sinclair,106,35000.00
2122,Three-Way Lines,105,30000.00
2120,Rico Enterprises,102,50000.00
2106,Fred Lewis Corp.,102,65000.00
2119,Solomon Inc.,109,25000.00
2118,Midwest Systems,108,60000.00
2113,Ian & Schmidt,104,20000.00
2109,Chen Associates,103,25000.00
2105,AAA Investments,101,45000.00

/ # python /tmp/13-sql-injectat.py  "select * from oficinas;"
Password for user postgres: 
22,Denver,Oeste,108,300000.00,186042.00
11,New York,Este,106,575000.00,692637.00
12,Chicago,Este,104,800000.00,735042.00
13,Atlanta,Este,105,350000.00,367911.00
21,Los Angeles,Oeste,108,725000.00,835915.00

/ # python /tmp/14-popen-sql-multi.py -c 2103 -c 2107
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00
```

  * Exemples '**nocius**' de SQL Injectat

```
/ # python /tmp/13-sql-injectat.py "select * from oficinas; drop table oficinas;"
Password for user postgres:


/ # python /tmp/13-sql-injectat.py  "select * from oficinas;"
Password for user postgres: 
ERROR:  relation "oficinas" does not exist
LINE 1: select * from oficinas;


/ # python /tmp/14-popen-sql-multi.py -c 2103 -c "2107;drop table pedidos;"
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00

/ # python /tmp/13-sql-injectat.py  "select * from pedidos;"
Password for user postgres: 
ERROR:  relation "pedidos" does not exist
LINE 1: select * from pedidos;


/ # python /tmp/14-popen-sql-multi.py -c 2103 -c "2107;drop database training;"
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00
##nota: no elimina la base de dades
```


### LDAP amb persistència de dades i de configuració

Aquest exercici porposa engegar un container amb la base de dades d’exemple usada a l’escola però desant en dos volums per una banda les dades de configuració i per altre les dades LDAP.

Dades:
  * Imatge ldap: edtasixm06/ldap21:grup
  * Directori de configuració: /etc/openldap/slapd.d (fedora) /etc/ldap (debian)
  * Directori de dades: /var/lib/ldap
  * Port: 389


  * Engegar un servido LDAP de prova amb les dades d’exemple de clase.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -d edtasixm06/ldap21:grup
Unable to find image 'edtasixm06/ldap21:grup' locally
grup: Pulling from edtasixm06/ldap21
955615a668ce: Already exists 
7fbf2bf4e0f8: Pull complete 
683633037b55: Pull complete 
cf56d4e28151: Pull complete 
94375ff90fd6: Pull complete 
b255624dcebc: Pull complete 
Digest: sha256:1194742a1ccb9ae55e660b6704cc3737d8d690259f7dfac90cdacb8fff1e476f
Status: Downloaded newer image for edtasixm06/ldap21:grup
feebbcee53cd51ea764a9369004e67d897ad584cad911f3335744a4975357b57

$ docker ps
CONTAINER ID   IMAGE                    COMMAND                  CREATED         STATUS         PORTS                                   NAMES
feebbcee53cd   edtasixm06/ldap21:grup   "/bin/sh -c /opt/doc…"   6 seconds ago   Up 4 seconds   0.0.0.0:389->389/tcp, :::389->389/tcp   ldap.edt.org
```

  * Verifiquem que el servidor ldap va i té dades.

```
$ docker exec -it ldap.edt.org ldapsearch -x -h 172.17.0.2 -b 'dc=edt,dc=org' | grep dn
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=grups,dc=edt,dc=org
dn: uid=pau,ou=usuaris,dc=edt,dc=org
dn: uid=pere,ou=usuaris,dc=edt,dc=org
dn: uid=anna,ou=usuaris,dc=edt,dc=org
dn: uid=marta,ou=usuaris,dc=edt,dc=org
dn: uid=jordi,ou=usuaris,dc=edt,dc=org
dn: uid=admin,ou=usuaris,dc=edt,dc=org
dn: uid=user01,ou=usuaris,dc=edt,dc=org
dn: uid=user02,ou=usuaris,dc=edt,dc=org
dn: uid=user03,ou=usuaris,dc=edt,dc=org
dn: uid=user04,ou=usuaris,dc=edt,dc=org
dn: uid=user05,ou=usuaris,dc=edt,dc=org
dn: uid=user06,ou=usuaris,dc=edt,dc=org
dn: uid=user07,ou=usuaris,dc=edt,dc=org
dn: uid=user08,ou=usuaris,dc=edt,dc=org
dn: uid=user09,ou=usuaris,dc=edt,dc=org
dn: uid=user10,ou=usuaris,dc=edt,dc=org
dn: cn=professors,ou=grups,dc=edt,dc=org
dn: cn=alumnes,ou=grups,dc=edt,dc=org
dn: cn=1asix,ou=grups,dc=edt,dc=org
dn: cn=2asix,ou=grups,dc=edt,dc=org
dn: cn=wheel,ou=grups,dc=edt,dc=org
dn: cn=1wiam,ou=grups,dc=edt,dc=org
dn: cn=2wiam,ou=grups,dc=edt,dc=org
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
```

  * Aturem el container.

```
$ docker stop ldap.edt.org 
```

  * Crear els volums.

```
$ docker volume create ldap-config 
ldap-config

$ docker volume create ldap-data
Ldap-data

$ docker volume ls
DRIVER    VOLUME NAME
local     ldap-config
local     ldap-data
```

  * Engegar el container amb persistencia de dades de la configuració i de les dades ldap.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -d edtasixm06/ldap21:grup
e3fe1ca9615b00e04b7678544e6adb1d839366777cb45299bcb42568000932b5

$ docker ps
CONTAINER ID   IMAGE                    COMMAND                  CREATED         STATUS         PORTS                                   NAMES
e3fe1ca9615b   edtasixm06/ldap21:grup   "/bin/sh -c /opt/doc…"   2 seconds ago   Up 2 seconds   0.0.0.0:389->389/tcp, :::389->389/tcp   ldap.edt.org
```

  * Llistar l’usuari pau i eliminar-lo.

```
$ docker exec -it ldap.edt.org ldapsearch -x -LLL -h 172.17.0.2 -b 'dc=edt,dc=org' uid=pau dn 
dn: uid=pau,ou=usuaris,dc=edt,dc=org

$ docker exec -it ldap.edt.org ldapdelete  -vx -h 172.17.0.2  -D 'cn=Manager,dc=edt,dc=org' -w secret 'uid=pau,ou=usuaris,dc=edt,dc=org'
ldap_initialize( ldap://172.17.0.2 )
deleting entry "uid=pau,ou=usuaris,dc=edt,dc=org"
```

  * Aturar el servei LDAP, el container s’elimina i crear-ne un de nou. Observar que l’usuari pau no existeix.

```
$ docker stop ldap.edt.org 
ldap.edt.org
```

**Nota**: en aquest exemple l’usuari hi tornarà a ser perquè el procés de creació de la imatge ldap usada sempre planxa de nou tota la base de dades. Seria més apropiat usar la base de dades amb entrypoint edtasixm06/ldap20:entrypoint.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -v ldap-config:/etc/openldap/slapd.d -v ldap-data:/var/lib/ldap -d edtasixm06/ldap20:entrypoint initdbedt
```




