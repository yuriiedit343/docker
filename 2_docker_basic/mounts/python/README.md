# apartats
## @edt ASIX-M05 Curs 2021-2022


### Exemples d'utilització de mounts: bind mounts i volumes

 * Scripts de bash per corregir

 * Programes python per corregir


#### Exemple corregir exercicis scripting de bash (root)


 * **scripts** Exemple de directori amb scripts de shell de linux que s'han 
   d'executar com a roor. El professor els ha de corregir en un entorn 
   'controlat'

```
$ cp -ra scripts/ /tmp/scripts
$ docker run --rm -v /tmp/scripts:/opt/examen -it debian /bin/bash

root@593afad5b756:/# cd /opt/examen/

root@593afad5b756:/opt/examen# ls
anna.sh  marta.sh  pau.sh  pere.sh

root@593afad5b756:/opt/examen# bash marta.sh 
aquest script funciona a la perfecció

root@593afad5b756:/opt/examen# bash pere.sh 
aquest script s'executa com a root i esborra tot /etc sense voler

root@593afad5b756:/opt/examen# exit
exit
```


#### Exemple programes python per corregir

 * **python** Exemple de directori amb programes python que cal provar
   en un entorn 'controlat'.

```
$ docker run --rm -v /tmp/python:/opt/pyexam -it python:3 /bin/bash

root@74cfefbbf631:/# cd /opt/pyexam/

root@74cfefbbf631:/opt/pyexam# ls
anna.py  marta.py  pau.py  pere.py

root@74cfefbbf631:/opt/pyexam# python pau.py 
Aquest programa fa el que toca de manera segura

root@74cfefbbf631:/opt/pyexam# python marta.py 
Genera un troyà per permetre l'entrada remota al sistema amb un exploit
L'invers modular de 3 mòdul 7 és 5
root@74cfefbbf631:/opt/pyexam# exit
exit
```

```
$ docker run --rm -v /tmp/python:/opt/pyexam -it python:3 python /opt/pyexam/pere.py
Aquest programa fa el que toca però desconfigura el sistema

$ docker run --rm -v /tmp/python:/opt/pyexam -it python:3 python /opt/pyexam/anna.py
Traceback (most recent call last):
  File "/opt/pyexam/anna.py", line 3, in <module>
    print("Linvers modular de 45 en mòdul 99 és", pow(45,-1,99))
ValueError: base is not invertible for the given modulus
```




