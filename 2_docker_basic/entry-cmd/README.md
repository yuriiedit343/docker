# docker
## @edt ASIX-M05 Curs 2021-2022


### Entrypoint / CMD

Exemples fets per usar en les explicacions dels conceptes *entrypoint* i *cmd*.


   * **cmd** imatge amb un cmd *date* per observar el funcionament de cmd.

   * **entry** imatge amb un entrypoint *date* per observar el funcionament de entry.

   * **entryopc** Afegit un exemple amb un script *opcions.sh* d'entrypoint per fer dia,
     o sysinfo o calendari.

   * **entrybd**  Afegit un escript d'exemple de entrypoint que simula una imatge amb 
     un gestor de base de dades amb les opcions initdb, destroy, dump, start.

---

# CMD / Entrypoint

Anomenem CMD a la ordre que s’ha d’executar en la creació d’un container, la ordre que té el PID 1. Aquesta ordre pot estar indicada al final de tot de l’ordre docker run o pot no indicar-se perquè la imatge ja té definida quina és l’ordre  per defecte (si no se n’indica cap) amb la directiva CMD del Dockerfile.


En canvi quan es defineix un entrypoint el container només pot executar aquella ordre i ‘argument que es passa al dicnal del docker run no pot ser una ordre sinó que són arguments que es passen a l’ordre definida en l’entrypoint.


## Valor per defecte de CMD

Algunes imatges tenen definida una ordre per defecte a executar en la directiva CMD. En aquests casos no és necessari posar l’ordre al final del docker run. Quan no hi ha ordre definida llavors és obligatori que docker run acabi amb l’ordre que s’utilitzarà per engegar el container i tindrà el PID 1.

  * Exemple executar un Fedora:27:
    * Primerament s’executa date de manera desatesa. No ha calgut posar -it perquè s’executa date i plega, no fem realment una sessió interactiva en un terminal.
    * En executar el fedora:27 sense ordre es mostra un error perquè no sap què executar. El Fedora:27 no té definit un CMD per defecte i per tant falla.
    * En executar com a ordre un shell /bin/bash ens falla perquè no hem pensat a posar el -it i per tant no iniciem la sessió interactiva.
    * Finalment si posem -it i el shell podem treballar interactivament dins el Fedora:27

```
$ docker run --rm  fedora:27 date 
Wed Jul  6 20:06:03 UTC 2022

$ docker run --rm  fedora:27 
docker: Error response from daemon: No command specified.
See 'docker run --help'.

$ docker run --rm  fedora:27 /bin/bash

$ docker run --rm  -it fedora:27 /bin/bash
[root@aa6f2361c70a /]# id
uid=0(root) gid=0(root) groups=0(root)
[root@aa6f2361c70a /]# exit
exit
```

  * Si consultem el docker history de la imatge Fedora:27 veiem que no té CMD, no té ordre per defecte, per això és obligatori posar-la al docker run.

```
$ docker history fedora:27 
IMAGE          CREATED       CREATED BY                                      SIZE      COMMENT
f89698585456   3 years ago   /bin/sh -c #(nop) ADD file:1307fb57471f2c791…   236MB     
<missing>      3 years ago   /bin/sh -c #(nop)  ENV DISTTAG=f27container …   0B        
<missing>      3 years ago   /bin/sh -c #(nop)  MAINTAINER [Adam Miller <…   0B      
```

  * Anem ara a veure amb un debian:latest
    * Primerament executem date desatesament, sense necessitat de posar -it.
    * Després executem debian sense ordre i sense -it i no genera error però no hi podem treballar, falta -it.
    * Per últim posant -it però sense ordre final en el docker run veiwm que inicia una sessió interactiva. Això és perquè per defecte aquesta imatge de debian té definit com a CMD l’ordre /bin/bash.

```
$ docker run --rm debian date
Wed Jul  6 20:11:13 UTC 2022

$ docker run --rm debian 

$ docker run --rm -it debian 
root@beb6b4be6be4:/# pwd
/
root@beb6b4be6be4:/# exit
exit
```

  * Si observem les capes de la imatge debian veiem que hi ha un CMD definit per defecte.

```
$ docker history debian:latest 
IMAGE          CREATED         CREATED BY                                      SIZE      COMMENT
82bd5ee7b1c5   10 months ago   /bin/sh -c #(nop)  CMD ["bash"]                 0B        
<missing>      10 months ago   /bin/sh -c #(nop) ADD file:1fedf68870782f1b4…   124MB     
```

  * Observem que el mateix passa en un alpine o un fedora:32.

```
$ docker run --rm -it fedora:32
[root@3c6b5625cedf /]# exit
exit

$ docker run --rm -it alpine
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
2408cc74d12b: Already exists 
Digest: sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c
Status: Downloaded newer image for alpine:latest
/ # exit

$ docker history fedora:32 
IMAGE          CREATED         CREATED BY                                      SIZE      COMMENT
c451de0d2441   14 months ago   /bin/sh -c #(nop)  CMD ["/bin/bash"]            0B        
<missing>      14 months ago   /bin/sh -c #(nop) ADD file:2825a5543f1544841…   202MB     
<missing>      15 months ago   /bin/sh -c #(nop)  ENV DISTTAG=f32container …   0B        
<missing>      15 months ago   /bin/sh -c #(nop)  LABEL maintainer=Clement …   0B        
 
$ docker history alpine:latest 
IMAGE          CREATED       CREATED BY                                      SIZE      COMMENT
e66264b98777   6 weeks ago   /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B        
<missing>      6 weeks ago   /bin/sh -c #(nop) ADD file:8e81116368669ed3d…   5.53MB    
```

  * Observem que també passa amb la nostra imatge mylinux i myden¡bian.

```
$ docker run --rm -it edtasixm05/myfedora
[root@b00bd18b7f0e /]# exit
exit
 
$ docker run --rm -it edtasixm05/mydebian
root@c2c8981d51a8:/tmp# exit
exit
 
$ docker history edtasixm05/myfedora:latest 
IMAGE          CREATED         CREATED BY                                      SIZE      COMMENT
6c3b8d6fdfe5   32 hours ago    /bin/bash                                       313MB     
<missing>      14 months ago   /bin/sh -c #(nop)  CMD ["/bin/bash"]            0B        
<missing>      14 months ago   /bin/sh -c #(nop) ADD file:2825a5543f1544841…   202MB     
<missing>      15 months ago   /bin/sh -c #(nop)  ENV DISTTAG=f32container …   0B        
<missing>      15 months ago   /bin/sh -c #(nop)  LABEL maintainer=Clement …   0B        

$ docker history edtasixm05/mydebian:latest 
IMAGE          CREATED         CREATED BY                                      SIZE      COMMENT
3ac951c9cb4e   27 hours ago    /bin/sh -c #(nop)  CMD ["/bin/sh" "-c" "/bin…   0B        
<missing>      27 hours ago    /bin/sh -c #(nop) WORKDIR /tmp                  0B        
<missing>      27 hours ago    /bin/sh -c #(nop) COPY multi:df2d144c45f31ee…   78B       
<missing>      27 hours ago    /bin/sh -c mkdir /normativa                     0B        
<missing>      27 hours ago    /bin/sh -c apt-get update && apt-get -y inst…   92.8MB    
<missing>      28 hours ago    /bin/sh -c #(nop)  LABEL curs=docker / AWS      0B        
<missing>      28 hours ago    /bin/sh -c #(nop)  LABEL author=@edt ASIX Cu…   0B        
<missing>      10 months ago   /bin/sh -c #(nop)  CMD ["bash"]                 0B        
<missing>      10 months ago   /bin/sh -c #(nop) ADD file:1fedf68870782f1b4…   124MB     
```

## Diferència CMD i Entrypoint

Per poder veure les diferències entre usar CMD i entrypoint utilitarian els exemples preparats dels apunts al GitLab de docker. Si no els hem descarregat els podem descarregar i anar al directory pertinent.

Es tracta d’observar que:

  * Un CMD indica l’ordre amb que s’ha d’iniciar el container. Si s’indica al final del docker run aquesta ordre té prioritat sobre el CMD per defecte definit en el Dockerfile.

  * Un entrypoint és una ordre que s’executara si o si com a ordre d’arrancada del container (PID 1) s’indiqui el que s’indiqui al final de l’ordre docker run. Quan la imatge s’ha definit amb ENTRYPOINT l’argument final de docker run no és una ordre sinó els arguments a passar a l’ordre de l’entrypoint.

  * Primerament anem a descarregar (si no s’ha fet ja) els apunts i anar al directori on provar els exemples. Anirem provant un per un els directoris.

```
$ cd /tmp/

$ git clone https://www.gitlab.com/edtasixm05/docker.git
Cloning into 'docker'...
warning: redirecting to https://gitlab.com/edtasixm05/docker.git/
remote: Enumerating objects: 640, done.
remote: Counting objects: 100% (462/462), done.
remote: Compressing objects: 100% (448/448), done.
remote: Total 640 (delta 209), reused 0 (delta 0), pack-reused 178
Receiving objects: 100% (640/640), 3.64 MiB | 6.48 MiB/s, done.
Resolving deltas: 100% (285/285), done.


$ cd /tmp/docker/entry-cmd/

$ pwd
/tmp/docker/entry-cmd

$ tree
.
├── cmd
│   ├── Dockerfile
│   └── README.md
├── entry
│   ├── Dockerfile
│   └── README.md
├── entrybd
│   └── README.md
├── entryopc
│   ├── Dockerfile
│   ├── opcions.sh
│   └── README.md
└── README.md
```

### Exemple directori cmd

  * Anar al directori i generar la imatge test-cmd que indica el Dockerfile.
    * Podem veure que mostra com fer-la en Fedora (comentada) i en Debian (la que generarà).
    * Aquesta imatge instal3la algun paquet i defineix el CMD per defecte date.
    * Per tant sempre que s’executi un container basat en aquesta imatge i no s’indica la ordre a executar al final del docker run s’executarà l’ordre date.
    * Però si s’indica una altra ordre s’executarà aquesta altra ordre. 

```
$ cat Dockerfile 
#FROM fedora:27
#RUN dnf -y install util-linux  nmap iputils
#CMD [ "date" ]

FROM debian
RUN apt-get update && apt-get -y install bsdmainutils
CMD [ "date" ]
```

  * Generem la imatge.

```
$ pwd
/tmp/docker/entry-cmd/cmd

$ ls
Dockerfile  README.md

$ docker build -t test-cmd .
Sending build context to Docker daemon  3.584kB
Step 1/3 : FROM debian
 ---> 82bd5ee7b1c5
Step 2/3 : RUN apt-get update && apt-get -y install bsdmainutils
 ---> Using cache
 ---> 265d3aa3fcbe
Step 3/3 : CMD [ "date" ]
 ---> Running in c8b81e62b8a4
Removing intermediate container c8b81e62b8a4
 ---> 61833044f7b3
Successfully built 61833044f7b3
Successfully tagged test-cmd:latest
```

  * La provem, provem els casos amb i sense ordre al docker run.

```
$ docker run --rm -it test-cmd
Wed Jul  6 20:31:37 UTC 2022

$ docker run --rm -it test-cmd cal
     July 2022        
Su Mo Tu We Th Fr Sa  
                1  2  
 3  4  5  6  7  8  9  
10 11 12 13 14 15 16  
17 18 19 20 21 22 23  
24 25 26 27 28 29 30  
31                    

$ docker run --rm -it test-cmd /bin/bash
root@143d89a8334d:/# id
uid=0(root) gid=0(root) groups=0(root)
root@143d89a8334d:/# exit
exit
```

### Exemple directory entry

  * Anar al directory d’exemple anomenat entry i generar la imatge que descriu el dockerfile. Ara es defineix un ENTRYPOINT i no un CMD. Indica l’ordre que executarà el container si o si i, l’ordre date.

```
$ cd ../entry

$ pwd
/tmp/docker/entry-cmd/entry

$ ls
Dockerfile  README.md
```

```
#FROM fedora:27
#RUN dnf -y install util-linux
#ENTRYPOINT ["date"]

FROM debian
RUN apt-get update && apt-get -y install bsdmainutils
ENTRYPOINT [ "date" ]
```

  * Generem una imatge anomenada test-entry.

```
$ docker build -t test-entry .
Sending build context to Docker daemon  4.608kB
Step 1/3 : FROM debian
 ---> 82bd5ee7b1c5
Step 2/3 : RUN apt-get update && apt-get -y install bsdmainutils
 ---> Using cache
 ---> 265d3aa3fcbe
Step 3/3 : ENTRYPOINT [ "date" ]
 ---> Using cache
 ---> 6b2f637702b0
Successfully built 6b2f637702b0
Successfully tagged test-entry:latest
```

  * Provem els casos on no pasem ordre al final del docker run i on passem altres ordres.
    * Observer que si no s’indica res eecuta date.
    * Però si indiquem altres ordres no les executa i mostra error. El que passa és que Docker interpreta les ordres cal i /bin/bash com a arguments de l’ordre date i intenta executar “date cal” i “date /bin/bash” i això genera errror.

```
$ docker run --rm -it test-entry
Wed Jul  6 20:37:35 UTC 2022

$ docker run --rm -it test-entry cal
date: invalid date 'cal'

$ docker run --rm -it test-entry /bin/bash
date: invalid date '/bin/bash'
```

  * Si hi ha definit un entrypoint allò que es passa com argument final del docker run són arguments que s’afegeixen al final de l’ordre de l’entrypoint, en el nostra cas arguments de l’ordre date. 
    * Provem d’executar date +%F
    * O d’executar date +”avui és %d-%m-%Y”. 
    * Podem veure que en tots dos casos la part final de l’ordre docker run ja no és una ordre sinó arguments de date.

```
$ docker run --rm -it test-entry +%F
2022-07-06

$ docker run --rm -it test-entry +"avui és %d-%m-%Y"
avui és 06-07-2022
```

  * En cas de necessitat podem modificar en el docker run quin és l’entrypoint, per exemple per iniciar una sessió interactiva dins el container per depurar coses.

```
$ docker run --rm --entrypoint /bin/bash -it test-entry 
root@1981e1c49c3f:/# id
uid=0(root) gid=0(root) groups=0(root)
root@1981e1c49c3f:/# exit
exit
```


### Exemple directori entryopc

  * Fem actiu el directori on hi ha aquests exemples i generem la imatge indicada. Aquest exemple utilitza un entrypoint que és un script amb opcions.

  * Segons siguin els arguments que es passen al final de l’ordre docker run l’escript de l’entrypoint actuarà d’una manera o d’una altra. En aquest exemple:
    * **sysinfo** si s’indica auest argument realitza l’ordre cat /etc/os-release.
    * **calendari** si s’indica aquest opció executa l’ordre cal.
    * **Qualsevol altra opció** mostra la data.

  * Així, doncs, s’ha creat una imatge on segons l’argument passat al docker run en la creació del container actuarà d’una manera o d’una altra. Aquest és un mecanisme per poder fer inicialitzacions diferents del conatiner.

```
$ cd entryopc/

$ pwd
/tmp/docker/entry-cmd/entryopc

$ ls
Dockerfile  opcions.sh  README.md
```

```
FROM debian
RUN apt-get update && apt-get -y install bsdmainutils
COPY ./opcions.sh /usr/bin/opcions.sh
RUN chmod +x /usr/bin/opcions.sh
ENTRYPOINT ["opcions.sh"]
```

```
$ cat opcions.sh 
#! /bin/bash

case $1  in
  "sysinfo")
      cat /etc/os-release;;
  "calendari")
      cal;;
  *)
      date;;	  
esac
```

  * Generem la imatge amb el mateix nom que abans.

```
$ docker build -t test-entry .
Sending build context to Docker daemon   5.12kB
Step 1/5 : FROM debian
 ---> 82bd5ee7b1c5
Step 2/5 : RUN apt-get update && apt-get -y install bsdmainutils
 ---> Using cache
 ---> 265d3aa3fcbe
Step 3/5 : COPY ./opcions.sh /usr/bin/opcions.sh
 ---> b6398695b90b
Step 4/5 : RUN chmod +x /usr/bin/opcions.sh
 ---> Running in b9ae9e24444d
Removing intermediate container b9ae9e24444d
 ---> a2544c6467a9
Step 5/5 : ENTRYPOINT ["opcions.sh"]
 ---> Running in 3fc0e6972573
Removing intermediate container 3fc0e6972573
 ---> 478f8850bbc3
Successfully built 478f8850bbc3
Successfully tagged test-entry:latest
```

  * I la provem.

```
$ docker run --rm -it test-entry
Wed Jul  6 20:50:44 UTC 2022
 
$ docker run --rm -it test-entry patata
Wed Jul  6 20:50:48 UTC 2022

$ docker run --rm -it test-entry calendari
     July 2022        
Su Mo Tu We Th Fr Sa  
                1  2  
 3  4  5  6  7  8  9  
10 11 12 13 14 15 16  
17 18 19 20 21 22 23  
24 25 26 27 28 29 30  
31                    

$ docker run --rm -it test-entry sysinfo
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```

### Exemple directory entrydb

  * Fer actiu aquest directory i generar la imatge amb el Dockerfile. Aquesta imatge defineix un script de entrypoint que simula un pocés de creació d’un container servidor de base de dades (similar al postgres). Segons l’argument rebut fa:
    * **initdb** inicialitza de nou la base de dades.
    * **destroy** elimina la base de dades.
    * **dump** fa un volcat de la base de dades.
    * **En qualsevol altre cas** engega el servidor de base de dades.

```
$ pwd
/tmp/docker/entry-cmd/entrybd

$ ls
Dockerfile  opcions.sh  README.md
```

```
$ cat Dockerfile 
FROM debian
RUN apt-get update && apt-get -y install bsdmainutils
COPY ./opcions.sh /usr/bin/opcions.sh
RUN chmod +x /usr/bin/opcions.sh
ENTRYPOINT ["opcions.sh"]
```

```
$ cat opcions.sh 
#! /bin/bash

case $1  in
 initdb)
    echo "Icinicialitzant la base de dades";;
 destroy)
    echo "Eliminant la base de dades";;
 dump)
    echo "Volcat de la base de dades";;
 *) 
    echo "Engegant la base de dades";;
esac  
```

```
$ docker build -t test-entry .
Sending build context to Docker daemon  4.608kB
Step 1/5 : FROM debian
 ---> 82bd5ee7b1c5
Step 2/5 : RUN apt-get update && apt-get -y install bsdmainutils
 ---> Using cache
 ---> 265d3aa3fcbe
Step 3/5 : COPY ./opcions.sh /usr/bin/opcions.sh
 ---> dc09ca4626d9
Step 4/5 : RUN chmod +x /usr/bin/opcions.sh
 ---> Running in 896a7df6cde0
Removing intermediate container 896a7df6cde0
 ---> 50f9b4c71ef4
Step 5/5 : ENTRYPOINT ["opcions.sh"]
 ---> Running in 8fbf8a7fd5d5
Removing intermediate container 8fbf8a7fd5d5
 ---> c0c7052b34d5
Successfully built c0c7052b34d5
Successfully tagged test-entry:latest
```

```
$ docker run -it test-entry initdb
Icinicialitzant la base de dades

$ docker run -it test-entry dump
Volcat de la base de dades

$ docker run -it test-entry destroy
Eliminant la base de dades

$ docker run -it test-entry 
Engegant la base de dades

$ docker run -it test-entry patata
Engegant la base de dades
```


### Exemple imatge oficial Postgres

En aquest exemple anem a veure que la imatge oficial de postgres utilitza com a entrypoint un script de GNU/Linux per decidir com inicialitza la base de dades.

Podem observar el Git Oficial de Postgres a:
	[docker-library/postgres](https://github.com/docker-library/postgres)

Ens fixem que té dos tipus de Dockerfile un per crear un alpine i un per crear un debian. Podem veure el Debian a:
	[Dockerfile-debia.template](https://github.com/docker-library/postgres/blob/master/Dockerfile-debian.template) 

I veiem que dins el fitxer l’entrada ENTRUPOINT diu:
	ENTRYPOINT ["docker-entrypoint.sh"]

I si examinem aquest fitxer veiem que és un script GNU/inux
	[docker-entrypoint.sh](https://github.com/docker-library/postgres/blob/master/docker-entrypoint.sh)


### Exemple servidor LDAP de classe amb entrypoint

Aquest exemple mostra un servidor LDAP dels que utilitzem a l’escola d’exemple que en lloc de inicialitzar sempre de nou la base de dades en crear el container té definit un script de entrypoint que permet seleccionar com crear el container.

GitHub  [ldap20:entrypoint](https://github.com/edtasixm06/ldap20/tree/master/ldap20:entrypoint)

Dockerfile
```
# ldapserver
FROM fedora:32
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="ldapserver"
RUN dnf -y install openldap-servers openldap-clients 
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
EXPOSE 389
```

startup.sh
```
#! /bin/bash
ulimit -n 1024

function initdbedt(){
  rm -rf /etc/openldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  cp /opt/docker/DB_CONFIG /var/lib/ldap/.
  slaptest -f /opt/docker/slapd.conf -F /etc/openldap/slapd.d
  slapadd -F /etc/openldap/slapd.d -l /opt/docker/edt.org.ldif
  chown -R ldap.ldap /etc/openldap/slapd.d
  chown -R ldap.ldap /var/lib/ldap
  cp /opt/docker/ldap.conf /etc/openldap/ldap.conf
  /sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://" 
}

function initdb(){
  rm -rf /etc/openldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  cp /opt/docker/DB_CONFIG /var/lib/ldap/.
  slaptest -f /opt/docker/slapd.conf -F /etc/openldap/slapd.d
  chown -R ldap.ldap /etc/openldap/slapd.d
  chown -R ldap.ldap /var/lib/ldap
  cp /opt/docker/ldap.conf /etc/openldap/ldap.conf
  /sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://"	
}

function start(){
  /sbin/slapd -d0 -h "ldap:// ldaps:// ldapi://"
}

echo "startup: $1, $*"
case $1 in
  initdbedt)
    echo "initdbedt"
    initdbedt;;
  initdb)
    echo "initdb"
    initdb;;    
  start)
    echo "start"
    start;;
  *)
    echo "altre"      	  
    start;;
esac	
#bash /opt/docker/install.sh
#/sbin/slapd -d0 ldap:// ldaps:// ldapi://
```

















