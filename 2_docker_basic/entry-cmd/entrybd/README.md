# docker
## @edt ASIX-M05 Curs 2021-2022


### Entrypoint amb script d'opcions


Aquest exemple reresenta que generaria una imatge d'un serveidor 
de bases de dades, per exemple postgres. El entrypoint és un script
que accepta un argument que diu què cal fer, si crear de nou la base de dades, eliminar-la, fer un dump, etc.

Pot predre els valors:
 * initdb
 * destroy
 * dump
 * qualsevol altre valor engega la bd

```
$ docker build -t testentry .

$ docker run --rm -it testentry 
Engegant la base de dades
$ 
$ docker run --rm -it testentry initdb
Icinicialitzant la base de dades
$ 
$ docker run --rm -it testentry destroy
Eliminant la base de dades
$ 
$ docker run --rm -it testentry dump
Volcat de la base de dades
$ 
$ docker run --rm -it testentry patata
Engegant la base de dades
```
