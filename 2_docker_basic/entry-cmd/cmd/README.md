# docker
## @edt ASIX-M05 Curs 2021-2022


### CMD

La imatge generade conté l'ordre *date* de CMD. Si no s'indica
l'ordre a executar executa date, però si s'indica una ordre diferent executa aquesta altra ordre.

```
$ docker build -t testcmd .

$ docker run --rm -it testcmd
Tue Jun 28 17:13:47 UTC 2022

$ docker run --rm -it testcmd date
Tue Jun 28 17:14:15 UTC 2022
 
$ docker run --rm -it testcmd cal
      June 2022     
Su Mo Tu We Th Fr Sa
          1  2  3  4 
 5  6  7  8  9 10 11 
12 13 14 15 16 17 18 
19 20 21 22 23 24 25 
26 27 28 29 30       
```

```
$ docker run --rm -it testcmd /bin/bash
[root@ad816ae95efe /]# ls  
bin   dev  home  lib64       media  opt   root  sbin  sys  usr
boot  etc  lib   lost+found  mnt    proc  run   srv   tmp  var
[root@ad816ae95efe /]# exit
exit
```

