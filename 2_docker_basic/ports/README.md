# docker
## @edt ASIX-M05 Curs 2021-2022

# Publicació de ports

En apartats anteriors hem après a crear una imatge amb un servidor web apache que es pot executar com un container detach, en background i ofereix el servei web al port 80 del container. Però si al servei només si pot accedir des del propi host amfitrió generalment no té cap gràcia.  

L’objectiu dels containers és oferir serveis com si fossin serveis que ofereix el host amfitrió de manera que altres hosts externs es connecten al host anfitrió pensant-se que és un servidor web quan en realitat el servei web està dins del container.

Per poder fer això cal fer propagació de ports, que docker anomena publish, publicar el port. El port del container es propaga a un port del host amfitrió de manera que quan externament s’accedeix a aquest port en realitat està responent el container.

Per poder fer propagació de ports s’utilitza l’opció de docker run -p o -P que té els formats:

```
-p port-host:port-container
-p wildcard-ip-host:port-containet

-P
```

**Atenció**: recordeu que si utilitzem la xarxa docker per defcete el primer container que despleguem té l’adreça IP **172.17.0.2**. El host amfitrió té sempre l’adreça **172.17.0.1** en aquesta xarxa.


## Publicar ports concrets

Podem propagar el port del container a un port del host amfitrió que pot ser el mateix port o un de diferent. Es pot propagar a totes les interfícies del host amfitrió o fer el binding ( o Listen) només a unes determinades. I també es pot publicar el port a un port aleatori del host que decideixi Docker.  Anem a veure totes aquestes opcions.


### Publicar al port 80 del host el port 80 del container

En aquest exemple desplegarem al port 80 del host amfitrió el port 80 del container. Aquest port es publica a totes les interficies que tingui el host amfitrió. Aquest cas és el més usual i és la sintàxis que trobarem normalment.

  * Desplegar el container amb el servidor web detach publicant-se al port 80 del host amfitrió.

  * Observem amb docker ps que els publica per a totes les interfícies 0.0.0.0 en ipv4 i en ipv6 (::). El port del host és el 80 i el del container també.

```
$ docker run --rm --name web.edt.org -p 80:80 -d edtasixm05/myweb
bab7f3efb8a895491c803534e6763ff5cad69f5011c98c566d5d3286d1332907

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                               NAMES
bab7f3efb8a8   edtasixm05/myweb   "/bin/sh -c 'apachec…"   3 seconds ago   Up 2 seconds   0.0.0.0:80->80/tcp, :::80->80/tcp   web.edt.org

$ docker port web.edt.org 
80/tcp -> 0.0.0.0:80
80/tcp -> :::80
```

  * Observem que el port 80 del host amfitrió està obert a totes les seves interfícies: 	
    * Al localhost (127.0.0.1).
    * Al la interfície de xarxa (amb cable o wireless).
    * A la interfície virtual de docker (**docker0**) que per defecte té l’adreça **172.17.0.1** en el cantó del host amfitrió.
    * Si el host témés interfícies (virtuals de virtualbox, libvirt, docker,  etc també es publicaria el port 80).

```
$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:10 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000098s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

$ nmap 192.168.1.118
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:12 CEST
Nmap scan report for 192.168.1.118
Host is up (0.00010s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:12 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

  * Verifiquem en totes elles que es pot accedir al servei web.

```
$ telnet localhost 80
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.0  (premeu aquó dos returns)

HTTP/1.1 200 OK
Date: Tue, 05 Jul 2022 20:15:20 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Tue, 05 Jul 2022 17:56:10 GMT
ETag: "25-5e3129149c280"
Accept-Ranges: bytes
Content-Length: 37
Connection: close
Content-Type: text/html

pagina web de benvinguda, hola gent!
Connection closed by foreign host.

$ wget 192.168.1.118
--2022-07-05 22:16:01--  http://192.168.1.118/
Connecting to 192.168.1.118:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 37 [text/html]
Saving to: ‘index.html.1’

index.html.1                            100%[=============================================================================>]      37  --.-KB/s    in 0s      

2022-07-05 22:16:01 (2.92 MB/s) - ‘index.html.1’ saved [37/37]

$ rm index.html.1 
```

![image1](images/image1.png)

```
```

![image2](images/image2.png)

```
```
![image3](images/image3.png)

```
```

  * **Atenció**: Observeu que no es poden engegar dos containers que publiquin el mateix port al host, a cada port del host només hi pot haver un servei o container escoltant. Si engegem un segon container que publiqui al port 80 del host amfitrió es produeix un conflicte.

    Aquest és un error molt típic de debutant, sempre que aparegui hem de pensar que el problema és que el port està ocupat. O bé hi ha un altre container que l’utilitza o bé a l’ordenador hi tenim engegat algun servei que l’utilitza.

```
$ docker run --rm --name web2.edt.org -p 80:80 -d edtasixm05/myweb
ad41b64bafedf7224dd92052caa6685ca6649ab7c23a2e4078ee04748f94580d
docker: Error response from daemon: driver failed programming external connectivity on endpoint web2.edt.org (f6404b3e263f2397205fb5cee0e33994776e04464ac3d5fb5487a3067643f364): Bind for 0.0.0.0:80 failed: port is already allocated.
```

  * Aturem el servei, el container detach. Com que hi hem posat l’opció --rm el container s’eliminarà.

```
$ docker stop web.edt.org 
web.edt.org

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```


### Publicar al host amfitrió a un port diferent del del container (5000:80)

En aquest exemple veurem com propagar al port 5000 del host amfitrió (podem escollir el port que volguem que no estigui ocupat) el port 80 del container.

  * Engegem amb docker run el servei web detach propagant el port.

```
$ docker run --rm --name web.edt.org -p 5000:80 -d edtasixm05/myweb
13beb8fbe0d34a67799e912cecc878e0ec774778706a634f5628376fec05a9c7

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                                   NAMES
13beb8fbe0d3   edtasixm05/myweb   "/bin/sh -c 'apachec…"   3 seconds ago   Up 3 seconds   0.0.0.0:5000->80/tcp, :::5000->80/tcp   web.edt.org

$ docker port web.edt.org 
80/tcp -> 0.0.0.0:5000
80/tcp -> :::5000
```

  * Observem les interfícies del host, en totes elles s’ha obert el port 5000.

```
$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:26 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00011s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp

$ nmap 192.168.1.118
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:26 CEST
Nmap scan report for 192.168.1.118
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp

$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:26 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp
```

```
$ telnet localhost 5000
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Tue, 05 Jul 2022 20:27:38 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Tue, 05 Jul 2022 17:56:10 GMT
ETag: "25-5e3129149c280"
Accept-Ranges: bytes
Content-Length: 37
Connection: close
Content-Type: text/html

pagina web de benvinguda, hola gent!
Connection closed by foreign host.
```

![image4](images/image4.png)
```
```
![image5](images/image5.png)
```
```
![image6](images/image6.png)
```
```

 * Aturem el servei, el container detach.

```
$ docker stop web.edt.org 
web.edt.org

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```


### Publicar un port a interficies del host determinades

En tots els exemple anteriors s’ha publicat el port a totes les interfícies. Recordem que en realitat l’opció -p és:

```
-p wilcard-ip-host:port-host:port-container
```

En els exemples anteriprs hem fet -p 80:80 i -p 5000:80, en tots dos casos no hem indicat la primera part que és a quines interfícies s’ha de publicar el port. Quan no s’indica es pren per defecte el valor **0.0.0.0** que és un **wildcard** que significa a totes les interfícies.

L’ordre docker port ens ho mostra clarament en els dos exemples anteriors:

```
$ docker port web.edt.org 
80/tcp -> 0.0.0.0:80
80/tcp -> :::80

$ docker port web.edt.org 
80/tcp -> 0.0.0.0:5000
80/tcp -> :::5000
```

Per tant dir -p 5000:80 és el mateix que dir en ipv4 -p 0.0.0.0:5000:80  i en ipv6 és ::5000:80.

Si es vol publicar el port només a una de les interfícies del host amfitrió cal indicar només la seva adreça IP. Si es vol publicar per varies interfícies caldrà usar més d’una vegada l’opció -p.


  * Publicar al port 5000 només del localhost. Observeu que a les dues altres interfícies no apareix publicat.

```
$ docker run --rm --name web.edt.org -p 127.0.0.1:5000:80 -d edtasixm05/myweb
6751c5698428e77f7f97a041bb3d334dbedbc9664ba0ea1f6dfbeddc5fd75169

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                    NAMES
6751c5698428   edtasixm05/myweb   "/bin/sh -c 'apachec…"   3 seconds ago   Up 2 seconds   127.0.0.1:5000->80/tcp   web.edt.org

$ docker port web.edt.org 
80/tcp -> 127.0.0.1:5000
```

```
$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:38 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00012s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp
```

```
$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:39 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00011s latency).
All 1000 scanned ports on 172.17.0.1 are closed

$ nmap 192.168.1.118
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:39 CEST
Nmap scan report for 192.168.1.118
Host is up (0.00011s latency).
All 1000 scanned ports on 192.168.1.118 are closed
```

```
$ docker stop web.edt.org 
```


  * Publicar al port 80 del host amfitrió de la interfície pública

```
$ docker run --rm --name web.edt.org -p 192.168.1.118:80:80 -d edtasixm05/myweb
13d24c742c9e11fcc2c21d77aa4cda6d8aa62932c004a7fb28c6214dc8bdb59c

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                      NAMES
13d24c742c9e   edtasixm05/myweb   "/bin/sh -c 'apachec…"   3 seconds ago   Up 2 seconds   192.168.1.118:80->80/tcp   web.edt.org

$ docker port web.edt.org 
80/tcp -> 192.168.1.118:80
```

```
$ nmap 192.168.1.118
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:42 CEST
Nmap scan report for 192.168.1.118
Host is up (0.00012s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:42 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00010s latency).
Other addresses for localhost (not scanned): ::1
All 1000 scanned ports on localhost (127.0.0.1) are closed

$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:43 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00011s latency).
All 1000 scanned ports on 172.17.0.1 are closed
```

```
$ docker stop web.edt.org 
```


  * Publicar a la interficie localhost i a la docker0.

```
$ docker run --rm --name web.edt.org -p 127.0.0.1:80:80 -p 172.17.0.1:80:80 -d edtasixm05/myweb
1c27bf5c32db66b969dee5bcace4965687280fde99b93508e9324ac016d83cf5

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                                         NAMES
1c27bf5c32db   edtasixm05/myweb   "/bin/sh -c 'apachec…"   6 seconds ago   Up 5 seconds   127.0.0.1:80->80/tcp, 172.17.0.1:80->80/tcp   web.edt.org

$ docker port web.edt.org 
80/tcp -> 172.17.0.1:80
80/tcp -> 127.0.0.1:80
```

```
$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:50 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00012s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:50 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

$ nmap 192.168.1.118
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:50 CEST
Nmap scan report for 192.168.1.118
Host is up (0.00011s latency).
All 1000 scanned ports on 192.168.1.118 are closed
```


## Publicar ports per defecte al host amfitrió

Amb docker run i l’opció **-P** podem indicar a Docker que publiqui el port del container a un port aleatori trait per docker. Usualment aquest opció s’utilitza conjuntament amb la directiva **EXPOSE** del fitxer Dockerfile.

Recodem el Dockerfile
```
# servidor web basat en debian:latest usant apache 
FROM debian:latest
LABEL author="@edt ASIX 2022"
LABEL curs="docker /AWS"
RUN apt-get update && apt-get install -y procps iproute2 iputils-ping nmap tree vim apache2
COPY index.html /var/www/html/
WORKDIR /tmp
EXPOSE 80
CMD  apachectl -k start -X 
```

La directiva EXPOSE no fa res per ella mateixa, no publica res, perquè això és en la creació de la imatge i els ports es publiquen realment en crear el container en fer el docker run. I per a què serveis, doncs?. Serveix per declarar la intenció de que la imatge utilitza i exposa el port indicat.

Quan s’utilitza en combinació amb l’opció -P del docker run llavors no cal indicar quin és el port del container que es publica, i Docker assigna automàticament un port dinàmic.

```
$ docker run --rm --name web.edt.org -P -d edtasixm05/myweb
a259e5207869bba3ded8194d7fe9cbb467bfb29da977fe0688e585668c243a7c

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS                                     NAMES
a259e5207869   edtasixm05/myweb   "/bin/sh -c 'apachec…"   5 seconds ago   Up 3 seconds   0.0.0.0:49153->80/tcp, :::49153->80/tcp   web.edt.org

$ docker port web.edt.org 
80/tcp -> 0.0.0.0:49153
80/tcp -> :::49153
```

  * Podem verificar que el servei web funciona en el port dinàmic de l’exemple 49153  totes les interfícies.

```
$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:57 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00013s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT      STATE SERVICE
49153/tcp open  unknown

$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:57 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00010s latency).
Not shown: 999 closed ports
PORT      STATE SERVICE
49153/tcp open  unknown

$ nmap 192.168.1.118
Starting Nmap 7.80 ( https://nmap.org ) at 2022-07-05 22:57 CEST
Nmap scan report for 192.168.1.118
Host is up (0.00010s latency).
Not shown: 999 closed ports
PORT      STATE SERVICE
49153/tcp open  unknown
```

![image7](images/image7.png)
```
```
![image8](images/image8.png)
```
```
![image9](images/image9.png)
```
```











