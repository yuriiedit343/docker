# docker
## @edt ASIX-M05 Curs 2021-2022

### Documentació

 * **[HowTo-ASIX-Docker.pdf](https://gitlab.com/edtasixm05/docker/-/raw/master/HowTo-ASIX-Docker.pdf)** 
   HowTo compler de iniciació a docker, descriu el procediment
   per aprendre a treballar en docker cobrint la part de 1_docker_inicial, 2_docker_basic i
   també els desplegament de docker_intermig.

 * **[HowTo-ASIX-Docker-Cloud-Machine-Swarm.pdf](https://gitlab.com/edtasixm05/docker/-/raw/master/HowTo-ASIX-Docker-Cloud-Machine-Swarm.pdf)**
   HowTo general d'explicació de què és Docker, les seves tecnologies i exemples del Get Started.

 * **[resum_ordres.md](https://gitlab.com/edtasixm05/docker/-/blob/master/resum_ordres.md)**
   Petita xuleta amb el resum de les principals ordres usades.

---

### Apunts i exemples pràctics de Docker

```
Docker Inicial:
  1 - Ordres Generals: imatges i containers
  2 - Crear imatges: commit, build i tag
  3 - Microserveis detach
  4 - Propagació (publicació) de ports

Docker Bàsic:
  1 - Mounts: Bind & Volume
  2 - Entry Points & CMD
  3 - Network
  4 - Environment variables
  5 - Docker secrets

Docker Intermig:
  1 - Compose
  2 - Deploy
  3 - Swarm / nodes & play-with-docker

GetStarted
  1 - Comptador
  2 - Bulletin Board
  3 - Comptador de visites-2
  4 - Getstarted Swarm
  5 - Items To-Do app

Dockefile

  1 - mydebian
  2 - myweb
  3 - web21
  4 - ssh21
  5 - net21
  6 - postgres21
  7 - ldap21
```

---

Copyright (c) 2022 Eduard Canet (per a tot el repositori)

Tot el programari que forma part d'aquest repositori és lliure amb llicència GPL v3: This is free software, licensed under the GNU General Public License v3. See this link for more info for more information.


La resta de documentació està subjecta a una llicència de Reconeixement-CompartirIgual 4.0 Internacional de Creative Commons que podeu trobar en aquest enllaç.

---
