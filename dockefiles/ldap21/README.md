# docker
## @edt ASIX-M05 Curs 2021-2022

# Servidor Ldap


Aquesta pràca consisteix en crear un servidor Open LDAP amb una base de dades ldap d’exemple.

Requeriments:

  * Identificar el nom dels paquets del servidor open LDAP i del client LDAP.
  * Obtenir el fitxer LDIF amb la informació de les entitats de la base de dades.
  * Obtenir la configuració del servidor LDAP.
  * Identificar el procés generació de la base de dades LDAP i de la inserció a baix nivell de les dades.
  * Identificar l’executable o l’ordre per engegar el servidor en foreground.
  * Important, identificar l’opció que l’engega en foreground.

```
$ pwd
/var/tmp/m05/docker/dockefiles/ldap21

$ ls
Dockerfile  edt.org.ldif  ldap.conf  slapd.conf  startup.sh
```

```
$ cat Dockerfile 
# ldapserver
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="ldapserver"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim ldap-utils slapd 
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

```
$ cat edt.org.ldif 
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectclass: organizationalunit

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectclass: organizationalunit

dn: ou=productes,dc=edt,dc=org
ou: productes
description: Container per a productes linux
objectclass: organizationalunit

dn: ou=usuaris,dc=edt,dc=org
ou: usuaris
description: Container per usuaris del sistema linux
objectclass: organizationalunit

dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per a grups del sistema linux
objectclass: organizationalunit

# ------- usuaris -------------------

dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 601
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

dn: uid=pere,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 601
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+

dn: uid=anna,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homephone: 555-222-2222
mail: anna@edt.org
description: Watch out for this girl
ou: Alumnes
uid: anna
uidNumber: 5002
gidNumber: 600
homeDirectory: /tmp/home/anna
userPassword: {SSHA}Bm4B3Bu/fuH6Bby9lgxfFAwLYrK0RbOq

dn: uid=marta,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marta Mas
sn: Mas
homephone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: Alumnes
uid: marta
uidNumber: 5003
gidNumber: 600
homeDirectory: /tmp/home/marta
userPassword: {SSHA}9+1F2f5vcW8z/tmSzYNWdlz5GbDCyoOw

dn: uid=jordi,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homephone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: Alumnes
ou: Profes
uid: jordi
uidNumber: 5004
gidNumber: 601
homeDirectory: /tmp/home/jordi
userPassword: {SSHA}T5jrMgpJwZZgu0azkLIVoYhiG08/KGUv

dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 10
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3

dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user01
cn: alumne01 de 1asix de todos los santos
sn: alumne01
homephone: 555-222-0001
mail: user01@edt.org
description: alumne de 1asix
ou: 1asix
uid: user01
uidNumber: 7001
gidNumber: 610
homeDirectory: /tmp/home/1asix/user01
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user02,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user02
cn: alumne02 Pou Prat
sn: alumne02
homephone: 555-222-0002
mail: user02@edt.org
description: alumne de 1asix
ou: 1asix
uid: user02
uidNumber: 7002
gidNumber: 610
homeDirectory: /tmp/home/1asix/user02
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user03,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user03
cn: alumne03 Pou Prat
sn: alumne03
homephone: 555-222-0003
mail: user03@edt.org
description: alumne de 1asix
ou: 1asix
uid: user02
uidNumber: 7003
gidNumber: 610
homeDirectory: /tmp/home/1asix/user03
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user04,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user04
cn: alumne04 Pou Puig
sn: alumne04
homephone: 555-222-0004
mail: user04@edt.org
description: alumne de 1asix
ou: 1asix
uid: user04
uidNumber: 7004
gidNumber: 610
homeDirectory: /tmp/home/1asix/user04
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user05,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user05
cn: alumne05 Pou Prat
sn: alumne05
homephone: 555-222-0005
mail: user05@edt.org
description: alumne de 1asix
ou: 1asix
uid: user05
uidNumber: 7005
gidNumber: 610
homeDirectory: /tmp/home/1asix/user05
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

# -------------------------------------------------------------
dn: uid=user06,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user06
cn: alumne06 de 2asix de todos los santos
sn: alumne06
homephone: 555-222-0006
mail: user06@edt.org
description: alumne de 2asix
ou: 2asix
uid: user06
uidNumber: 7006
gidNumber: 611
homeDirectory: /tmp/home/2asix/user06
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user07,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user07
cn: alumne07 de 2asix de todos los santos
sn: alumne07
homephone: 555-222-0007
mail: user07@edt.org
description: alumne de 2asix
ou: 2asix
uid: user07
uidNumber: 7007
gidNumber: 611
homeDirectory: /tmp/home/2asix/user07
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user08,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user08
cn: alumne08 de 2asix de todos los santos
sn: alumne08
homephone: 555-222-0008
mail: user08@edt.org
description: alumne de 2asix
ou: 2asix
uid: user08
uidNumber: 7008
gidNumber: 611
homeDirectory: /tmp/home/2asix/user08
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user09,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user09
cn: alumne09 de 2asix de todos los santos
sn: alumne09
homephone: 555-222-0009
mail: user09@edt.org
description: alumne de 2asix
ou: 2asix
uid: user09
uidNumber: 7009
gidNumber: 611
homeDirectory: /tmp/home/2asix/user09
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

dn: uid=user10,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user10
cn: alumne10 de 2asix de todos los santos
sn: alumne10
homephone: 555-222-0016
mail: user10@edt.org
description: alumne de 2asix
ou: 2asix
uid: user10
uidNumber: 7010
gidNumber: 611
homeDirectory: /tmp/home/2asix/user10
userPassword: {SHA}ovf8ta/reYP/u2zj0afpHt8yE1A=

# ----------- grups -----------------------------

dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Grup de professors
memberUid: pau
memberUid: pere
memberUid: jordi

dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: Grup de alumnes
memberUid: anna
memberUid: marta
memberUid: jordi

dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: Grup de 1asix
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05  

dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: Grup de 2asix
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10

dn: cn=wheel,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: wheel
gidNumber: 10
description: Grup de wheel
memberUid: admin

dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: Grup de 1wiam

dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: Grup de 2wiam

dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Grup de 1hiaw
```

```
$ cat ldap.conf 
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt

BASE dc=edt,dc=org
URI  ldap://ldap.edt.org


$ cat slapd.conf 
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include		/etc/ldap/schema/corba.schema
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/duaconf.schema
include		/etc/ldap/schema/dyngroup.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/java.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
include		/etc/ldap/schema/ppolicy.schema
include		/etc/ldap/schema/collective.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw {SSHA}nGnnCLjjdiKky4o0swoYmTOW4F8cJOWq
# el passwd és: secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database config
rootdn "cn=Sysadmin,cn=config"
rootpw {SSHA}NAvfoSAIXhDag0Ucgkhmr02ah+8ld+yV
# el passwd es syskey
# -------------------------------------------------------------------
database monitor
```

```
$ cat startup.sh 
#! /bin/bash

# export DEBIAN_FRONTEND=noninteractive
# apt-get -y install slapd

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt.org.ldif
slapcat

chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0 
```

```
$ docker build -t prova-ldap21 .

$ docker run --rm --name ldap21 -p 389:389 -d prova-ldap21
fe1cbe60795cf28444a1c83a1eddcd24aa34bbfc71aa5a742211bbe7043387b6
$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS        PORTS                                   NAMES
fe1cbe60795c   prova-ldap21   "/bin/sh -c /opt/doc…"   3 seconds ago   Up 1 second   0.0.0.0:389->389/tcp, :::389->389/tcp   ldap21
```

```
$ ldapsearch -x -LLL -h localhost -b 'dc=edt,dc=org' -s base
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org
```

```
$ docker exec -it ldap21 ldapsearch -x -LLL -h localhost | grep dn
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=grups,dc=edt,dc=org
dn: uid=pau,ou=usuaris,dc=edt,dc=org
dn: uid=pere,ou=usuaris,dc=edt,dc=org
dn: uid=anna,ou=usuaris,dc=edt,dc=org
dn: uid=marta,ou=usuaris,dc=edt,dc=org
dn: uid=jordi,ou=usuaris,dc=edt,dc=org
dn: uid=admin,ou=usuaris,dc=edt,dc=org
dn: uid=user01,ou=usuaris,dc=edt,dc=org
dn: uid=user02,ou=usuaris,dc=edt,dc=org
dn: uid=user03,ou=usuaris,dc=edt,dc=org
dn: uid=user04,ou=usuaris,dc=edt,dc=org
dn: uid=user05,ou=usuaris,dc=edt,dc=org
dn: uid=user06,ou=usuaris,dc=edt,dc=org
dn: uid=user07,ou=usuaris,dc=edt,dc=org
dn: uid=user08,ou=usuaris,dc=edt,dc=org
dn: uid=user09,ou=usuaris,dc=edt,dc=org
dn: uid=user10,ou=usuaris,dc=edt,dc=org
dn: cn=professors,ou=grups,dc=edt,dc=org
dn: cn=alumnes,ou=grups,dc=edt,dc=org
dn: cn=1asix,ou=grups,dc=edt,dc=org
dn: cn=2asix,ou=grups,dc=edt,dc=org
dn: cn=wheel,ou=grups,dc=edt,dc=org
dn: cn=1wiam,ou=grups,dc=edt,dc=org
dn: cn=2wiam,ou=grups,dc=edt,dc=org
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
```

```
$ docker stop ldap21 
ldap21 

$ docker rmi prova-ldap21:latest 
```




