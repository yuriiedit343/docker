# docker
## @edt ASIX-M05 Curs 2021-2022

# Servidor Postgres


En aquesta pràctica es construeix un servidor postgres amb les dades de la base de dades training ja carregades. S’utilitzarà com a imatge base la imatge oficial de postgrees i es copia les dades de training i els scripts d’inicialització al directori d’inicialització de postgres.


Requeriments:

  * Identificar el nom de la imatge postgres oficial.
  * Obtenir les dades de la base de dades training i els scripts d’inicialització.
  * Identificar el directori de postgres que permet automatitzar la inicialització.

```
$ pwd
./docker/dockefiles/postgres21

$ ls
Dockerfile  training
```

```
$ cat Dockerfile 
# Postgres amb base de dades training
FROM library/postgres
ADD . .
COPY training/* /docker-entrypoint-initdb.d/
```

```
$ docker build -t prova-postgres21 .

$ docker run --rm --name postgres21  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d prova-postgres21 
c2e84b3fe92ceb6c799feca22dc360a2218417ffa6e4360ac9f9f32ffe64eabe

$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED         STATUS         PORTS      NAMES
c2e84b3fe92c   prova-postgres21   "docker-entrypoint.s…"   2 seconds ago   Up 2 seconds   5432/tcp   postgres21
```

```
$ docker exec -it postgres21 psql  -U postgres -d training -c "select * from oficinas;"
 oficina |   ciudad    | region | dir | objetivo  |  ventas   
---------+-------------+--------+-----+-----------+-----------
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00
      11 | New York    | Este   | 106 | 575000.00 | 692637.00
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00
(5 rows)
```

```
$ docker stop postgres21 
postgres21

$ docker rmi prova-postgres21:latest 
```




