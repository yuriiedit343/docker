# docker
## @edt ASIX-M05 Curs 2021-2022

# Servidor Net

En aquesta pràctica es crearà el que anomenarem un servidor Net que no és més que el servei xinetd amb els seus serveis echo-stream (7), daytime-stream (13) i chargen-stream (19) activats.

Aquests serveis són:

  * **Echo (7)** es tracta d’un servei TCP que en connectar-si amb telnet fa un echo de tot el que rep. És un servei clàssic per provar la connectivitat client servidor. 

  * **Daytime (13)** es tracta d’un servei TCP que mostra la data i la hora actuals. Aquest servei és molt pràctic de connectar-si amb telnet perquè contesta de cop (no hi ha diàleg com amb el servei Echo) i verfica la connectivitat de xarxa.

  * **Chagen (19)** es tracta d’un servei TCP  una mica ‘locu’ ja que mostra per pantalla ininterrumpudament tot el joc de caràcters.  
  * **Nota**: per ptractivcar aquests serveis s’utilitza com a client l’ordre telnet host port. Per exemple telnet 172.17.0.2 7, telnet localhost 13 o telnet A.B.C.D 19.

  * **Atenció**: un problema usual és sortir del diàleg de telnet (amb echo i chargen). No es prem ^c (mai) sinó que cal prémer ^] (la tecla control i el claudator, però recordeu que per prémer el caudator cal prémer també AltGr). Uncop fet cal fer quit.


Requeriments:

  * Identificar el nom del paquet del servei xinetd.
  * Identificar els tres fitxers que permeten configurar el servei TCP de echo, daytime i chargen. Atenció que volem la configuració TCP i no la UPD.
  * A cada un d’aquests fitxers cal canviar on diu disable=yes per disable=no.
  * Copiar aquests fitxers (o editar-los) en el directori de configuració del servei, que cal identificar.
  * Identificar l’executable o l’ordre per engegar el servidor en foreground.
  * Important, identificar l’opció que l’engega en foreground.

```
$ pwd
./docker/dockefiles/net21

$ ls
chargen-stream  daytime-stream  Dockerfile  echo-stream
```

```
$ cat Dockerfile 
# net21:base
FROM debian
LABEL author="@edt ASIX-M05"
LABEL description="xinetd server Curs 2021-2022"
RUN apt-get update && apt-get -y install xinetd iproute2 iputils-ping nmap procps net-tools 
COPY daytime-stream chargen-stream echo-stream  /etc/xinetd.d/
WORKDIR /tmp
CMD [ "/usr/sbin/xinetd", "-dontfork" ]
EXPOSE 7 13 19
```

```
$ head echo-stream 
# This is the configuration for the tcp/stream echo service.

service echo
{
# This is for quick on or off of the service
	disable		= no

# The next attributes are mandatory for all services
	id		= echo-stream
	type		= INTERNAL
...
```

```
$ head daytime-stream 
# This is the configuration for the tcp/stream daytime service.

service daytime
{
# This is for quick on or off of the service
	disable		= no

# The next attributes are mandatory for all services
	id		= daytime-stream
	type		= INTERNAL
...
```

```
$ head chargen-stream 
# This is the configuration for the tcp/stream chargen service.

service chargen
{
# This is for quick on or off of the service
	disable		= no

# The next attributes are mandatory for all services
	id		= chargen-stream
	type		= INTERNAL
```

```
$ docker build -t prova-net21 .

$ docker run --rm --name net21 -p 7:7 -p 13:13 -p 19:19  -d prova-net21
16aa9b5f866573b9af34b72666703d328b232a0a6f685a94f4cd09eeec0a5620

$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED         STATUS         PORTS                                                                                                 NAMES
16aa9b5f8665   prova-net21   "/usr/sbin/xinetd -d…"   2 seconds ago   Up 2 seconds   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:19->19/tcp, :::19->19/tcp   net21


$ telnet localhost 13
Trying ::1...
Connected to localhost.
Escape character is '^]'.
07 JUL 2022 17:36:34 UTC
Connection closed by foreign host.

$ docker stop net21 
net21

$ docker rmi prova-net21:latest 
```




